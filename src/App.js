import React from "react";
//import { render } from "react-dom";
import Home from "./components/Home";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";


// function NotFound() {
//   return <h2>La pagina solicitada no existe</h2>;
// }

// const App = () => {
//   return (
//     <Router>
//       <div>
//         <div className="container" exact={true}>
//           <Routes>
//             <Route path="/" exact={true}>
//               <Home />
//             </Route>
//             <Route path="*">
//               <NotFound />
//             </Route>
//           </Routes>
//         </div>
//       </div>
//     </Router>
//   );
// };

// const appDiv = document.getElementById("app");
// render(<App />, appDiv);
// export default App;

function App() {
  return (
    <Router>
      <div>
        <div className="container" exact="true">
          <Routes>
            <Route path="/" element={<Home/>}/>
            {/* <Route path="*">
              <NotFound />
            </Route> */}
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;

// export default App;
// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
