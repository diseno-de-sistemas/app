import React, { useState, useEffect } from 'react'
import axios from "axios";
import MaterialTable from '@material-table/core';
import { Formik, Form, Field } from "formik";
import { Modal, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import "../Home.css";
import { FormGroup} from "@mui/material";
import jsPDF from "jspdf";
import "jspdf-autotable";
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
//Columnas de la tabla
const columns = [
    {
        title: "Codigo",
        field: "codigo",
        grouping: true,
        
    },
    {
      title: "Nombre",
      field: "nombre",
      grouping: true,
      
    },
    {
      title: "Fecha de Publicacion",
      field: "fecha_publicacion",
      grouping: true,
      sorting: false,
      
    },
    {
      title: "Tipo",
      field: "tipo",
      grouping: false,
      sorting: false,
    },
    {
        title: "Monto Otorgado",
        field: "monto_otorgado",
        grouping: true,
        sorting: false,
    },
    {
        title: "Detalle años",
        field: "detalle_anios",
        grouping: false,
        sorting: false,
        filtering: false,
    },
    {
        title: "Duracion",
        field: "duracion",
        grouping: false,
        sorting: false,
        filtering: false,
    },
    {
        title: "Numero de Empleados",
        field: "numero_empleados",
        grouping: false,
        sorting: false,
        filtering: false,
    },
    {
        title: "Informacion Adicional",
        field: "informacion_adicional",
        grouping: true,
        sorting: false,
    },
    {
        title: "Empresa",
        field: "razon_social",
        grouping: true,
        sorting: false,
    },
  ];
  const downloadPdf = (dat) => {
    const doc = new jsPDF()
    doc.text("Licitacions", 20, 10)
    doc.autoTable({
      theme: "grid",
      columns: columns.map(col => ({ ...col, dataKey: col.field })),
      body: dat
    })
    doc.save('Licitaciones.pdf')
  }
  //url de la api
const baseUrl = "http://45.56.114.13:8000/api/cp/licitacion/";
//Estilos del modal
const useStyles = makeStyles((theme) => ({
    modal: {
      position: "absolute",
      width: 500,
      backgroundColor: theme.palette.background.paper,
      border: "2px solid #000",
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
    },
    iconos: {
      cursor: "pointer",
    },
    FieldMaterial: {
      width: "100%",
      
    },
    ErrorMessage: {
      color:'#FF0000',
      fontStyle: "italic",
      fontFamily: "Times New Roman",
      fontSize: "15px",
      
    },
  }));
  
  //Estilos para la DataTable
  const tableStyles = makeStyles(theme => ({
    table: {
        marginTop: theme.spacing(3),
        '& thead th': {
            fontWeight: '600',
            color: theme.palette.primary.main,
            backgroundColor: " #d5d6dc ",
        },
        '& tbody td': {
            fontWeight: '300',
        },
        '& tbody tr:hover': {
            backgroundColor: '#fffbf2',
            cursor: 'pointer',
        },
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        borderBlockStyle: 'solid',
        borderBlockWidth: 'medium',
    },
  }))

function Licitaciones(){
    const styles = useStyles();
    const table = tableStyles();
    const [data, setData] = useState([]);
    const [modalInsertar, setModalInsertar] = useState(false);
    const [modalEditar, setModalEditar] = useState(false);
    const [modalEliminar, setModalEliminar] = useState(false);
    const [empresa, setEmpresa] = useState([]);
    const [licitacionSeleccionada, setLicitacionSeleccionada] = useState({
        id: "",
        codigo: "",
        nombre: "",
        fecha_publicacion: "",
        tipo: "",
        monto_otorgado: "",
        detalle_anios: "",
        duracion: "",
        numero_empleados: "",
        informacion_adicional: "",
        razon_social: "",
      });
      const handleChange = (e) => {
        const { name, value } = e.target;
        setLicitacionSeleccionada((prevState) => ({
        ...prevState,
        [name]: value,
        }));
    }; 
    //Trae los datos desde la api
    const peticionGet = async () => {
        await axios
            .get(baseUrl)
            .then((response) => {
                setData(response.data);
            })
            .catch((error) => {
                console.log(error);
        });
    };
    // Obtener empresas de BD
  const empresaGet = async () => {
    await axios
      .get("http://45.56.114.13:8000/api/cp/empresa/")
      .then((response) => {
        setEmpresa(response.data);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };
  const peticionPut = async (values) => {
    const headers = { "Content-Type": "application/json" };
    await axios
      .put(baseUrl + "modificar/" + licitacionSeleccionada.id + "/", licitacionSeleccionada,headers)
      .then((response) => {
        var dataNueva = data;
        dataNueva.map((licitacion) => {
          if (licitacion.id === licitacionSeleccionada.id) {
            licitacion.codigo = licitacionSeleccionada.codigo
            licitacion.nombre = licitacionSeleccionada.nombre;
            licitacion.fecha_publicacion = licitacionSeleccionada.fecha_publicacion;
            licitacion.tipo = licitacionSeleccionada.tipo;  
            licitacion.monto_otorgado = licitacionSeleccionada.monto_otorgado;
            licitacion.detalle_anios = licitacionSeleccionada.detalle_anios;
            licitacion.duracion = licitacionSeleccionada.duracion;
            licitacion.numero_empleados = licitacionSeleccionada.numero_empleados;
            licitacion.informacion_adicional = licitacionSeleccionada.informacion_adicional;
            licitacion.razon_social = licitacionSeleccionada.razon_social;           
          }
        });
        setData(dataNueva);
        abrirCerrarModalEditar();
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const peticionPost = async (values) => {
    const headers = { "Content-Type": "application/json" };
    await axios
      .post(baseUrl, 
        {
          codigo: values.codigo,
          nombre:values.nombre,
          fecha_publicacion: values.fecha_publicacion,
          tipo: values.tipo,
          monto_otorgado: values.monto_otorgado,
          detalle_anios: values.detalle_anios,
          duracion: values.duracion,
          numero_empleados: values.numero_empleados,
          informacion_adicional: values.informacion_adicional,
          razon_social: values.razon_social 
        },headers)
      .then((response) => {
        setData(data.concat(response.data));
        console.log(licitacionSeleccionada);
        abrirCerrarModalInsertar();
      })
      .catch((error) => {
        console.log(error);
    });
};
const peticionDelete = async () => {
    await axios
      .delete(baseUrl + "modificar/" + licitacionSeleccionada.id + "/")
      .then((response) => {
        setData(
          data.filter((licitacion) => licitacion.id !== licitacionSeleccionada.id)
        );
        abrirCerrarModalEliminar();
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const seleccionarLicitacion = (licitacion, caso) => {
    setLicitacionSeleccionada({
      id: licitacion.id, 
      codigo: licitacion.codigo, 
      nombre:licitacion.nombre,
      fecha_publicacion:licitacion.fecha_publicacion,
      tipo:licitacion.tipo,
      monto_otorgado:licitacion.monto_otorgado,
      detalle_anios: licitacion.detalle_anios,
      duracion:licitacion.duracion,
      numero_empleados:licitacion.numero_empleados,
      informacion_adicional:licitacion.informacion_adicional,
      razon_social:licitacion.razon_social
    });
    caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };

  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };

  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };

  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };
  useEffect(() => {
    peticionGet();
    empresaGet();
  }, []);

  useEffect(() => {
    if (modalInsertar === false || modalEditar === false)
      peticionGet();
  }, [modalInsertar, modalEditar,modalEliminar]);

  //Modal para insertar Proceso
  const [formularioEnviado, cambiarFormularioEnviado] = useState(false);
  const bodyInsertar = (
  <div className={styles.modal}>
    <h3>Nueva Licitacion</h3>
    <br/>
    <Formik
       initialValues={{
        codigo: "",
        nombre: "",
        fecha_publicacion: "",
        tipo: "",
        monto_otorgado: "",
        detalle_anios: "",
        duracion: "",
        numero_empleados: "",
        informacion_adicional: "",
        razon_social: "",
              }}
              validate={(values) => {
                  const errors = {};
        //Validaciones para evitar campos en blanco a la hora de enviar el formulario
        if(!values.codigo){
          errors.codigo = 'Por favor ingresar el codigo'
        }
        if(!values.fecha_publicacion){
          errors.fecha_publicacion = 'Por favor ingresar la fecha de publicacion'
        }
        if(!values.tipo){
          errors.tipo = 'Por favor ingresar el tipo'
        }
        if(!values.monto_otorgado){
          errors.monto_otorgado = 'Por favor ingresar el monto otorgado'
        }
        if(!values.informacion_adicional){
          errors.informacion_adicional = 'Por favor ingresar la informacion adicional'
        }
        if(!values.detalle_anios){
          errors.detalle_anios = 'Por favor ingresar el detalle en años'
        }
        if(!values.duracion){
          errors.duracion = 'Por favor ingresar el duracion'
        }
        if(!values.numero_empleados){
          errors.numero_empleados = 'Por favor ingresar el numero de empleados'
        }
        if(!values.razon_social){
          errors.razon_social = 'Por favor ingresar el nombre de la empresa'
        }
        return errors;
        }}
        onSubmit={(values, {resetForm}) => {
          peticionPost();
          resetForm();
          console.log('Formulario enviado');
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
            >
        {( {errors,touched,handleBlur,handleChange,handleSubmit,values } ) => (
                  <Form >
          <Grid container  spacing={3} >
            <Grid  item xs={6} >
            <FormGroup>
                <label htmlFor="codigo">Codigo</label>
                <Field
                      type="text" 
                      id="codigo" 
                      name="codigo" 
                      className={styles.FieldMaterial}
                      validate={errors.codigo && touched.codigo}
                      onChange={handleChange}
                      onBlur={handleBlur}      
                />
                { errors.codigo && touched.codigo && < div className={styles.ErrorMessage} > { errors.codigo } </ div > } 
              </FormGroup>
              <br/>
              <FormGroup>
                <label htmlFor="nombre">Nombre</label>
                <Field
                      type="text" 
                      id="nombre" 
                      name="nombre" 
                      className={styles.FieldMaterial}
                      validate={errors.nombre && touched.nombre}
                      onChange={handleChange}
                      onBlur={handleBlur}      
                />
                { errors.nombre&& touched.nombre && < div className={styles.ErrorMessage} > { errors.nombre } </ div > } 
              </FormGroup>
              <br/>
              <FormGroup>
                <label htmlFor="fecha_publicacion">Fecha de publicacion</label>
                <Field
                    type="date" 
                    id="fecha_publicacion" 
                    name="fecha_publicacion" 
                    className={styles.FieldMaterial}
                    validate={errors.fecha_publicacion && touched.fecha_publicacion}
                    onChange={handleChange}
                    onBlur={handleBlur}
                   
                />
                { errors.fecha_publicacion && touched.fecha_publicacion && < div className={styles.ErrorMessage} > { errors.fecha_publicacion } </ div > } 
              </FormGroup>
              <br/>
              <FormGroup>
                <label htmlFor="tipo">tipo</label>
                <Field
                    type="text" 
                    id="tipo" 
                    name="tipo" 
                    className={styles.FieldMaterial}
                    validate={errors.tipo && touched.tipo}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    
                />
                { errors.tipo && touched.tipo && < div className={styles.ErrorMessage} > { errors.tipo } </ div > }
              </FormGroup>
              <br/>
              <FormGroup>
                <label htmlFor="monto_otorgado">Monto Otorgado</label>
                <Field
                    type="number" 
                    pattern="[0-9]+([\.,][0-9]+)?" 
                    step="0.01"
                    id="monto_otorgado" 
                    name="monto_otorgado" 
                    className={styles.FieldMaterial}
                    validate={errors.monto_otorgado && touched.monto_otorgado}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    
                />
                { errors.monto_otorgado && touched.monto_otorgado && < div className={styles.ErrorMessage} > { errors.monto_otorgado } </ div > }
              </FormGroup>
              <br/>
              <FormGroup>
                <label htmlFor="duracion">duracion</label>
                <Field
                    type="number" 
                    id="duracion" 
                    name="duracion" 
                    className={styles.FieldMaterial}
                    validate={errors.duracion && touched.duracion}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    
                />
                { errors.duracion && touched.duracion && < div className={styles.ErrorMessage} > { errors.duracion } </ div > }
              </FormGroup>
              </Grid>
              <Grid  item xs={6} md={6} >
              <FormGroup>
                <label htmlFor="informacion_adicional">Informacion Adicional</label>
                <Field
                    type="text" 
                    id="informacion_adicional" 
                    name="informacion_adicional" 
                    className={styles.FieldMaterial}
                    validate={errors.informacion_adicional && touched.informacion_adicional}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    
                />
                { errors.informacion_adicional && touched.informacion_adicional && < div className={styles.ErrorMessage} > { errors.informacion_adicional } </ div > }
              </FormGroup>
              <br/>
              <FormGroup>
                <label htmlFor="detalle_anios">Detalle en años</label>
                <Field
                    type="number" 
                    id="detalle_anios" 
                    name="detalle_anios" 
                    className={styles.FieldMaterial}
                    validate={errors.detalle_anios && touched.detalle_anios}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    
                />
                { errors.detalle_anios && touched.detalle_anios && < div className={styles.ErrorMessage} > { errors.detalle_anios } </ div > }
              </FormGroup>
              <br/>
              <FormGroup>
                <label htmlFor="numero_empleados">Seleccione el numero de empleados</label>
                <Field
                    type="number"
                    id="numero_empleados" 
                    name="numero_empleados" 
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder="--- Seleccione institucion---"
                    validate={errors.numero_empleados && touched.numero_empleados}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    
                />
              
                { errors.numero_empleados && touched.numero_empleados && < div className={styles.ErrorMessage} > { errors.numero_empleados } </ div > }
              </FormGroup>
              <br/>
              <FormGroup>
                <label htmlFor="razon_social">Seleccion el nombre de emnombre_empresa</label>
                <Field
                    component='select'
                    id="razon_social" 
                    name="razon_social" 
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder="--- Seleccione emnombre_empresa---"
                    validate={errors.razon_social && touched.razon_social}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    
                >
              <option value="" className="text-secondary">
                    --- Seleccione empresa ---
                  </option>
                  {empresa.map((empresa) => (
                    <option
                      key={empresa.id}
                      value={empresa.id}
                      className={styles.FieldMaterial}
                    >
                      {empresa.razon_social}
                    </option> 
                    ))}     
              </Field>
                { errors.razon_social && touched.razon_social && < div className={styles.ErrorMessage} > { errors.razon_social} </ div > }
              </FormGroup>
              <br/><br/>
                <Stack direction="row" justifyContent='center' spacing={2}>
                  <Button variant="contained"  color="success" onClick={() => peticionPost(values)}>Insertar</Button>
                  <Button variant="contained"  color="error" onClick={() => abrirCerrarModalInsertar()}>Cancelar</Button>
                </Stack>                                                                       
              </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  </div>
);
//Modal para Editar Licitaciones
const bodyEditar = (
    <div className={styles.modal}>
      <h3>Editar Licitacion</h3>
      <br/>
      <Formik
         initialValues={{
          codigo: "",
          nombre: "",
          fecha_publicacion: "",
          tipo: "",
          monto_otorgado: "",
          detalle_anios: "",
          duracion: "",
          numero_empleados: "",
          informacion_adicional: "",
          razon_social: "",
                }}
                validate={(values) => {
                    const errors = {};
          //Validaciones para evitar campos en blanco a la hora de enviar el formulario
          if(!values.codigo){
            errors.codigo = 'Por favor ingresar el codigo'
          }
          if(!values.fecha_publicacion){
            errors.fecha_publicacion = 'Por favor ingresar la fecha de publicacion'
          }
          if(!values.tipo){
            errors.tipo = 'Por favor ingresar el tipo'
          }
          if(!values.monto_otorgado){
            errors.monto_otorgado = 'Por favor ingresar el monto otorgado'
          }
          if(!values.informacion_adicional){
            errors.informacion_adicional = 'Por favor ingresar la informacion adicional'
          }
          if(!values.detalle_anios){
            errors.detalle_anios = 'Por favor ingresar el detalle en años'
          }
          if(!values.duracion){
            errors.duracion = 'Por favor ingresar el duracion'
          }
          if(!values.numero_empleados){
            errors.numero_empleados = 'Por favor ingresar el numero de empleados'
          }
          if(!values.razon_social){
            errors.razon_social = 'Por favor ingresar el nombre de la empresa'
          }
          return errors;
          }}
          onSubmit={(values, {resetForm}) => {
            peticionPut();
            resetForm();
            console.log('Formulario enviado');
            cambiarFormularioEnviado(true);
            setTimeout(() => cambiarFormularioEnviado(false), 5000);
          }}
              >
          {( {errors,touched,handleBlur,handleSubmit,values } ) => (
                    <Form >
            <Grid container  spacing={3} >
              <Grid  item xs={6} >
              <FormGroup>
                  <label htmlFor="codigo">Codigo</label>
                  <Field
                        type="text" 
                        id="codigo" 
                        name="codigo" 
                        className={styles.FieldMaterial}
                        validate={errors.codigo && touched.codigo}
                        onChange={handleChange}
                        onBlur={handleBlur} 
                        value={licitacionSeleccionada &&licitacionSeleccionada.codigo}     
                  />
                  { errors.codigo && touched.codigo && < div className={styles.ErrorMessage} > { errors.codigo } </ div > } 
                </FormGroup>
                <br/>
                <FormGroup>
                  <label htmlFor="nombre">Nombre</label>
                  <Field
                        type="text" 
                        id="nombre" 
                        name="nombre" 
                        className={styles.FieldMaterial}
                        validate={errors.nombre && touched.nombre}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={licitacionSeleccionada &&licitacionSeleccionada.nombre}       
                  />
                  { errors.nombre&& touched.nombre && < div className={styles.ErrorMessage} > { errors.nombre } </ div > } 
                </FormGroup>
                <br/>
                <FormGroup>
                  <label htmlFor="fecha_publicacion">Fecha de publicacion</label>
                  <Field
                      type="date" 
                      id="fecha_publicacion" 
                      name="fecha_publicacion" 
                      className={styles.FieldMaterial}
                      validate={errors.fecha_publicacion && touched.fecha_publicacion}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={licitacionSeleccionada &&licitacionSeleccionada.fecha_publicacion} 
                  />
                  { errors.fecha_publicacion && touched.fecha_publicacion && < div className={styles.ErrorMessage} > { errors.fecha_publicacion } </ div > } 
                </FormGroup>
                <br/>
                <FormGroup>
                  <label htmlFor="tipo">tipo</label>
                  <Field
                      type="text" 
                      id="tipo" 
                      name="tipo" 
                      className={styles.FieldMaterial}
                      validate={errors.tipo && touched.tipo}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={licitacionSeleccionada &&licitacionSeleccionada.tipo} 
                  />
                  { errors.tipo && touched.tipo && < div className={styles.ErrorMessage} > { errors.tipo } </ div > }
                </FormGroup>
                <br/>
                <FormGroup>
                  <label htmlFor="monto_otorgado">Monto Otorgado</label>
                  <Field
                      type="number" 
                      pattern="[0-9]+([\.,][0-9]+)?" 
                      step="0.01"
                      id="monto_otorgado" 
                      name="monto_otorgado" 
                      className={styles.FieldMaterial}
                      validate={errors.monto_otorgado && touched.monto_otorgado}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={licitacionSeleccionada &&licitacionSeleccionada.monto_otorgado} 
                  />
                  { errors.monto_otorgado && touched.monto_otorgado && < div className={styles.ErrorMessage} > { errors.monto_otorgado } </ div > }
                </FormGroup>
                <br/>
                <FormGroup>
                  <label htmlFor="duracion">duracion</label>
                  <Field
                      type="number" 
                      id="duracion" 
                      name="duracion" 
                      className={styles.FieldMaterial}
                      validate={errors.duracion && touched.duracion}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={licitacionSeleccionada &&licitacionSeleccionada.duracion} 
                  />
                  { errors.duracion && touched.duracion && < div className={styles.ErrorMessage} > { errors.duracion } </ div > }
                </FormGroup>
                </Grid>
                <Grid  item xs={6} md={6} >
                <FormGroup>
                  <label htmlFor="informacion_adicional">Informacion Adicional</label>
                  <Field
                      type="text" 
                      id="informacion_adicional" 
                      name="informacion_adicional" 
                      className={styles.FieldMaterial}
                      validate={errors.informacion_adicional && touched.informacion_adicional}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={licitacionSeleccionada &&licitacionSeleccionada.informacion_adicional} 
                  />
                  { errors.informacion_adicional && touched.informacion_adicional && < div className={styles.ErrorMessage} > { errors.informacion_adicional } </ div > }
                </FormGroup>
                <br/>
                <FormGroup>
                  <label htmlFor="detalle_anios">Detalle en años</label>
                  <Field
                      type="number" 
                      id="detalle_anios" 
                      name="detalle_anios" 
                      className={styles.FieldMaterial}
                      validate={errors.detalle_anios && touched.detalle_anios}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={licitacionSeleccionada &&licitacionSeleccionada.detalle_anios} 
                  />
                  { errors.detalle_anios && touched.detalle_anios && < div className={styles.ErrorMessage} > { errors.detalle_anios } </ div > }
                </FormGroup>
                <br/>
                <FormGroup>
                  <label htmlFor="numero_empleados">Seleccion el nombre de institucion</label>
                  <Field
                      type="number"
                      id="numero_empleados" 
                      name="numero_empleados" 
                      multiple={false}
                      className={styles.FieldMaterial}
                      placeholder="--- Seleccione institucion---"
                      validate={errors.numero_empleados && touched.numero_empleados}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={licitacionSeleccionada &&licitacionSeleccionada.numero_empleados} 
                  />
                
                  { errors.numero_empleados && touched.numero_empleados && < div className={styles.ErrorMessage} > { errors.numero_empleados } </ div > }
                </FormGroup>
                <br/>
                <FormGroup>
                  <label htmlFor="razon_social">Seleccion el nombre de nombre_empresa</label>
                  <Field
                      component='select'
                      id="razon_social" 
                      name="razon_social" 
                      multiple={false}
                      className={styles.FieldMaterial}
                      placeholder="--- Seleccione emnombre_empresa---"
                      validate={errors.razon_social && touched.razon_social}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={licitacionSeleccionada &&licitacionSeleccionada.razon_social} 
                  >
                <option value="" className="text-secondary">
                      --- Seleccione empresa ---
                    </option>
                    {empresa.map((empresa) => (
                      <option
                        key={empresa.razon_social}
                        value={empresa.id}
                        className={styles.FieldMaterial}
                      >
                        {empresa.razon_social}
                      </option> 
                      ))}     
                </Field>
                  { errors.razon_social && touched.razon_social && < div className={styles.ErrorMessage} > { errors.razon_social } </ div > }
                </FormGroup>
                <br/><br/>
                  <Stack direction="row" justifyContent='center' spacing={2}>
                    <Button variant="contained"  color="success" onClick={() => peticionPut(values)}>Insertar</Button>
                    <Button variant="contained"  color="error" onClick={() => abrirCerrarModalEditar()}>Cancelar</Button>
                  </Stack>                                                                       
                </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );
  //modal para eliminar un proceso
const bodyEliminar=(
    <div className={styles.modal}>
      <p>Estás seguro que deseas eliminar la licitacion <b>{licitacionSeleccionada && licitacionSeleccionada.nombre}</b>? </p>
      <div align="right">
        <Button color="secondary" onClick={()=>peticionDelete()}>Sí</Button>
        <Button onClick={()=>abrirCerrarModalEliminar()}>No</Button>
      </div>
    </div>
  )
    return (
        <>
        <div className={table.table}>
          <MaterialTable
            columns={columns}
            data={data}
            title="Info de Licitaciones"
            //Propiedades de los iconos eliminar y editar
            actions={[
              {
                icon: () => <AddIcon />,
                tooltip: 'Agregar Licitacion',
                isFreeAction: true,
                onClick: (event, rowData) => abrirCerrarModalInsertar(),
               
              },
              {
                icon: () => <EditIcon />,
                tooltip: 'Editar Licitacion',
                onClick: (event, rowData) => seleccionarLicitacion(rowData, "Editar")
              },
              {
                icon: () => <DeleteIcon />,
                tooltip: 'Eliminar Licitacion',
                onClick: (event, rowData) => seleccionarLicitacion(rowData, "Eliminar")
              },
              {
                icon: () => <PictureAsPdfIcon />,// you can pass icon too
                tooltip: "Export to Pdf",
                onClick: () => downloadPdf(data),
                isFreeAction: true
              },
            ]}
            //Propiedades de la tabla
            options={{
              
              filtering: true,
              grouping: true,
              sorting: true,
              thirdSortClick: false,
              search: true,
              searchFieldAlignment: "right", 
              searchAutoFocus: true, 
              searchFieldVariant: "standard",
              paging: true,
              pageSizeOptions: [5, 10, 20, 25, 50, 100],
              pageSize: 5,
              paginationType: "stepped",
              showFirstLastPageButtons: false,
              paginationPosition: "bottom",
              addRowPosition: "first",
              actionsColumnIndex: -1,
              columnsButton: true,
            }}
            localization={{
              header:{
                actions: "Acciones"
              }
            }}
            
          />
          <Modal
          open={modalInsertar}
          onClose={abrirCerrarModalInsertar}>
            {bodyInsertar}
          </Modal>
  
          <Modal
          open={modalEditar}
          onClose={abrirCerrarModalEditar}>
            {bodyEditar}
          </Modal>
  
          <Modal
          open={modalEliminar}
          onClose={abrirCerrarModalEliminar}>
            {bodyEliminar}
          </Modal>
        </div>
  
      </>
        );
}

export default Licitaciones;