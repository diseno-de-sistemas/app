import React, { useState, useEffect } from "react";
import axios from "axios";
import MaterialTable from "@material-table/core";
import { Formik, Form, Field } from "formik";
import { Modal, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import "../Home.css";
import { FormGroup } from "@mui/material";
import jsPDF from "jspdf";
import "jspdf-autotable";
import PictureAsPdfIcon from "@mui/icons-material/PictureAsPdf";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
//Columnas de la tabla
const columns = [
  {
    title: "Nombre",
    field: "nombre",
    grouping: true,
  },
  {
    title: "Costo Base",
    field: "costo_base",
    grouping: true,
    sorting: false,
  },
  {
    title: "Estado",
    field: "estado",
    grouping: false,
    sorting: false,
  },
  {
    title: "Tipo de Servicio",
    field: "tipo_servicio",
    grouping: true,
    sorting: false,
  },
  {
    title: "Informacion Adicional",
    field: "informacion_adicional",
    grouping: false,
    sorting: false,
    filtering: false,
  },
  {
    title: "Indicaciones Generales",
    field: "indicaciones_generales",
    grouping: false,
    sorting: false,
    filtering: false,
  },
  {
    title: "Objeto",
    field: "objeto",
    grouping: false,
    sorting: false,
    filtering: false,
  },
  {
    title: "Institucion",
    field: "nombre_institucion",
    grouping: true,
    sorting: false,
  },
  {
    title: "Uaci",
    field: "nombre_uaci",
    grouping: true,
    sorting: false,
  },
];

const downloadPdf = (dat) => {
  const doc = new jsPDF();
  doc.text("Procesos", 20, 10);
  doc.autoTable({
    theme: "grid",
    columns: columns.map((col) => ({ ...col, dataKey: col.field })),
    body: dat,
  });
  doc.save("Procesoes.pdf");
};
//url de la api
const baseUrl = "http://45.56.114.13:8000/api/cp/procesos/";
//Estilos del modal
const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 500,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  FieldMaterial: {
    width: "100%",
  },
  ErrorMessage: {
    color: "#FF0000",
    fontStyle: "italic",
    fontFamily: "Times New Roman",
    fontSize: "15px",
  },
}));

//Estilos para la DataTable
const tableStyles = makeStyles((theme) => ({
  table: {
    marginTop: theme.spacing(3),
    "& thead th": {
      fontWeight: "600",
      color: theme.palette.primary.main,
      backgroundColor: " #d5d6dc ",
    },
    "& tbody td": {
      fontWeight: "300",
    },
    "& tbody tr:hover": {
      backgroundColor: "#fffbf2",
      cursor: "pointer",
    },
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderBlockStyle: "solid",
    borderBlockWidth: "medium",
  },
}));

function Procesos() {
  const styles = useStyles();
  const table = tableStyles();
  const [data, setData] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [institucion, setinstitucion] = useState([]);
  const [uaci, setUaci] = useState([]);
  const [procesoSeleccionada, setProcesoSeleccionada] = useState({
    id: "",
    nombre: "",
    costo_base: "",
    estado: "",
    tipo_servicio: "",
    informacion_adicional: "",
    indicaciones_generales: "",
    objeto: "",
    nombre_institucion: "",
    nombre_uaci: "",
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setProcesoSeleccionada((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  //Trae los datos desde la api
  const peticionGet = async () => {
    await axios
      .get(baseUrl)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  // Obtener intituciones de BD
  const institucionesGet = async () => {
    await axios
      .get("http://45.56.114.13:8000/api/cp/instituciones/")
      .then((response) => {
        setinstitucion(response.data);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };

  // Obtener Uaci de BD
  const uaciGet = async () => {
    await axios
      .get("http://45.56.114.13:8000/api/cp/uaci/")
      .then((response) => {
        setUaci(response.data);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };

  const peticionPost = async (values) => {
    console.log("VALUES: ", values);
    const headers = { "Content-Type": "application/json" };
    await axios
      .post(
        baseUrl,
        {
          nombre: values.nombre,
          costo_base: values.costo_base,
          estado: values.estado,
          tipo_servicio: values.tipo_servicio,
          informacion_adicional: values.informacion_adicional,
          indicaciones_generales: values.indicaciones_generales,
          objeto: values.objeto,
          id_institucion: values.nombre_institucion,
          id_uaci: values.nombre_auci,
        },
        headers
      )
      .then((response) => {
        setData(data.concat(response.data));
        // console.log(procesoSeleccionada);
        abrirCerrarModalInsertar();
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const peticionPut = async () => {
    const headers = { "Content-Type": "application/json" };
    // console.log(procesoSeleccionada.id_uaci);
    await axios
      .put(
        baseUrl + "modificar/" + procesoSeleccionada.id + "/",
        {
          nombre: procesoSeleccionada.nombre,
          costo_base: procesoSeleccionada.costo_base,
          estado: procesoSeleccionada.estado,
          tipo_servicio: procesoSeleccionada.tipo_servicio,
          informacion_adicional: procesoSeleccionada.informacion_adicional,
          indicaciones_generales: procesoSeleccionada.indicaciones_generales,
          objeto: procesoSeleccionada.objeto,
          id_institucion: procesoSeleccionada.id_institucion,
          id_uaci: procesoSeleccionada.id_uaci,
        },
        headers
      )
      .then((response) => {
        var dataNueva = data;
        dataNueva.map((proceso) => {
          if (proceso.id === procesoSeleccionada.id) {
            proceso.nombre = procesoSeleccionada.nombre;
            proceso.costo_base = procesoSeleccionada.costo_base;
            proceso.estado = procesoSeleccionada.estado;
            proceso.tipo_servicio = procesoSeleccionada.tipo_servicio;
            proceso.informacion_adicional =
              procesoSeleccionada.informacion_adicional;
            proceso.indicaciones_generales =
              procesoSeleccionada.indicaciones_generales;
            proceso.objeto = procesoSeleccionada.objeto;
            proceso.nombre_institucion = procesoSeleccionada.nombre_institucion;
            proceso.nombre_uaci = procesoSeleccionada.nombre_uaci;
          }
        });
        setData(dataNueva);
        abrirCerrarModalEditar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const peticionDelete = async () => {
    await axios
      .delete(baseUrl + "modificar/" + procesoSeleccionada.id + "/")
      .then((response) => {
        setData(
          data.filter((proceso) => proceso.id !== procesoSeleccionada.id)
        );
        abrirCerrarModalEliminar();
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const seleccionarProceso = (proceso, caso) => {
    setProcesoSeleccionada(proceso);
    caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };

  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };

  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };

  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  useEffect(() => {
    peticionGet();
    institucionesGet();
    uaciGet();
  }, []);

  useEffect(() => {
    if (modalInsertar === true || modalEditar === true) peticionGet();
  }, [modalInsertar, modalEditar, modalEliminar]);

  useEffect(() => {
    if (modalInsertar === false || modalEditar === false) peticionGet();
  }, [modalInsertar, modalEditar]);

  //Modal para insertar Proceso
  const [formularioEnviado, cambiarFormularioEnviado] = useState(false);
  const bodyInsertar = (
    <div className={styles.modal}>
      <h3>Nuevo Proceso</h3>
      <br />
      <Formik
        initialValues={{
          nombre: "",
          costo_base: "",
          estado: "",
          tipo_servicio: "",
          informacion_adicional: "",
          indicaciones_generales: "",
          objeto: "",
          nombre_institucion: "",
          nombre_uaci: "",
        }}
        validate={(values) => {
          const errors = {};
          //Validaciones para evitar campos en blanco a la hora de enviar el formulario
          if (!values.costo_base) {
            errors.costo_base = "Por favor ingresar el costo base";
          }
          if (!values.nombre) {
            errors.nombre = "Por favor ingresar la especializacion";
          }
          if (!values.estado) {
            errors.estado = "Por favor ingresar el estado";
          }
          if (!values.tipo_servicio) {
            errors.tipo_servicio = "Por favor ingresar el tipo de servicio";
          }
          if (!values.informacion_adicional) {
            errors.informacion_adicional =
              "Por favor ingresar la informacion adicional";
          }
          if (!values.indicaciones_generales) {
            errors.indicaciones_generales =
              "Por favor ingresar las indicaciones generales";
          }
          if (!values.objeto) {
            errors.objeto = "Por favor ingresar el objeto";
          }
          if (!values.nombre_institucion) {
            errors.nombre_institucion =
              "Por favor ingresar el nombre de institucion";
          }
          if (!values.nombre_uaci) {
            errors.nombre_uaci = "Por favor ingresar el nombre de uaci";
          }
          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          peticionPost();
          resetForm();
          console.log("Formulario enviado");
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          values,
        }) => (
          <Form>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor="nombre">Nombre</label>
                  <Field
                    type="text"
                    id="nombre"
                    name="nombre"
                    className={styles.FieldMaterial}
                    validate={errors.nombre && touched.nombre}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.nombre && touched.nombre && (
                    <div className={styles.ErrorMessage}> {errors.nombre} </div>
                  )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="costo_base">Costo Base</label>
                  <Field
                    type="text"
                    id="costo_base"
                    name="costo_base"
                    className={styles.FieldMaterial}
                    validate={errors.costo_base && touched.costo_base}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.costo_base && touched.costo_base && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.costo_base}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="estado">Estado</label>
                  <Field
                    type="text"
                    id="estado"
                    name="estado"
                    className={styles.FieldMaterial}
                    validate={errors.estado && touched.estado}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.estado && touched.estado && (
                    <div className={styles.ErrorMessage}> {errors.estado} </div>
                  )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="tipo_servicio">Tipo de servico</label>
                  <Field
                    component="select"
                    id="tipo_servicio"
                    name="tipo_servicio"
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder="--- Seleccione institucion---"
                    validate={errors.tipo_servicio && touched.tipo_servicio}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione el tipo de servicio ---
                    </option>
                    <option
                      key="Estado"
                      value="Estado"
                      className={styles.FieldMaterial}
                    >
                      Estado
                    </option>
                    <option
                      key="Obra"
                      value="Obra"
                      className={styles.FieldMaterial}
                    >
                      Obra
                    </option>
                    <option
                      key="Servicio"
                      value="Servicio"
                      className={styles.FieldMaterial}
                    >
                      Servicio
                    </option>
                  </Field>
                  {errors.nombre_institucion && touched.nombre_institucion && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.nombre_institucion}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="objeto">Objeto</label>
                  <Field
                    type="text"
                    id="objeto"
                    name="objeto"
                    className={styles.FieldMaterial}
                    validate={errors.objeto && touched.objeto}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.objeto && touched.objeto && (
                    <div className={styles.ErrorMessage}> {errors.objeto} </div>
                  )}
                </FormGroup>
              </Grid>
              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor="informacion_adicional">
                    Informacion Adicional
                  </label>
                  <Field
                    component="textarea"
                    id="informacion_adicional"
                    name="informacion_adicional"
                    className={styles.FieldMaterial}
                    validate={
                      errors.informacion_adicional &&
                      touched.informacion_adicional
                    }
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.informacion_adicional &&
                    touched.informacion_adicional && (
                      <div className={styles.ErrorMessage}>
                        {" "}
                        {errors.informacion_adicional}{" "}
                      </div>
                    )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="indicaciones_generales">
                    Indicaciones Generales
                  </label>
                  <Field
                    component="textarea"
                    id="indicaciones_generales"
                    name="indicaciones_generales"
                    className={styles.FieldMaterial}
                    validate={
                      errors.indicaciones_generales &&
                      touched.indicaciones_generales
                    }
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.indicaciones_generales &&
                    touched.indicaciones_generales && (
                      <div className={styles.ErrorMessage}>
                        {" "}
                        {errors.indicaciones_generales}{" "}
                      </div>
                    )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="nombre_institucion">
                    Seleccion el nombre de institucion
                  </label>
                  <Field
                    component="select"
                    id="nombre_institucion"
                    name="nombre_institucion"
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder="--- Seleccione institucion---"
                    validate={
                      errors.nombre_institucion && touched.nombre_institucion
                    }
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione institucion ---
                    </option>
                    {institucion.map((institucion) => (
                      <option
                        key={institucion.id}
                        value={institucion.id}
                        className={styles.FieldMaterial}
                      >
                        {institucion.nombre}
                      </option>
                    ))}
                  </Field>
                  {errors.nombre_institucion && touched.nombre_institucion && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.nombre_institucion}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="nombre_uaci">
                    Seleccion el nombre de uaci
                  </label>
                  <Field
                    component="select"
                    id="nombre_uaci"
                    name="nombre_auci"
                    multiple={false}
                    className={styles.FieldMaterial}
                    validate={errors.nombre_uaci && touched.nombre_uaci}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione uaci ---
                    </option>
                    {uaci.map((uaci) => (
                      <option
                        key={uaci.id}
                        value={uaci.id}
                        className={styles.FieldMaterial}
                      >
                        {uaci.nombre}
                      </option>
                    ))}
                  </Field>
                  {errors.nombre_uaci && touched.nombre_uaci && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.nombre_uaci}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <br />
                <Stack direction="row" justifyContent="center" spacing={2}>
                  <Button
                    variant="contained"
                    color="success"
                    onClick={() => peticionPost(values)}
                  >
                    Insertar
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={() => abrirCerrarModalInsertar()}
                  >
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );
  //Modal para Editar Proceso

  const bodyEditar = (
    <div className={styles.modal}>
      <h3>Editar Proceso</h3>
      <br />
      <Formik
        initialValues={{
          nombre: "",
          costo_base: "",
          estado: "",
          tipo_servicio: "",
          informacion_adicional: "",
          indicaciones_generales: "",
          objeto: "",
          nombre_institucion: "",
          nombre_uaci: "",
        }}
        validate={(values) => {
          const errors = {};
          //Validaciones para evitar campos en blanco a la hora de enviar el formulario
          if (!values.costo_base) {
            errors.costo_base = "Por favor ingresar el costo base";
          }
          if (!values.nombre) {
            errors.nombre = "Por favor ingresar la especializacion";
          }
          if (!values.estado) {
            errors.estado = "Por favor ingresar el estado";
          }
          if (!values.tipo_servicio) {
            errors.tipo_servicio = "Por favor ingresar el tipo de servicio";
          }
          if (!values.informacion_adicional) {
            errors.informacion_adicional =
              "Por favor ingresar la informacion adicional";
          }
          if (!values.indicaciones_generales) {
            errors.indicaciones_generales =
              "Por favor ingresar las indicaciones generales";
          }
          if (!values.objeto) {
            errors.objeto = "Por favor ingresar el objeto";
          }
          if (!values.nombre_institucion) {
            errors.nombre_institucion =
              "Por favor ingresar el nombre de institucion";
          }
          if (!values.nombre_uaci) {
            errors.nombre_uaci = "Por favor ingresar el nombre de uaci";
          }
          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          peticionPut();
          resetForm();
          console.log("Formulario enviado");
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({ errors, touched, handleBlur, handleSubmit, values }) => (
          <Form>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor="nombre">Nombre</label>
                  <Field
                    type="text"
                    id="nombre"
                    name="nombre"
                    className={styles.FieldMaterial}
                    validate={errors.nombre && touched.nombre}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={procesoSeleccionada && procesoSeleccionada.nombre}
                  />
                  {errors.nombre && touched.nombre && (
                    <div className={styles.ErrorMessage}> {errors.nombre} </div>
                  )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="costo_base">Costo Base</label>
                  <Field
                    type="text"
                    id="costo_base"
                    name="costo_base"
                    className={styles.FieldMaterial}
                    validate={errors.costo_base && touched.costo_base}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={
                      procesoSeleccionada && procesoSeleccionada.costo_base
                    }
                  />
                  {errors.costo_base && touched.costo_base && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.costo_base}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="estado">Estado</label>
                  <Field
                    type="text"
                    id="estado"
                    name="estado"
                    className={styles.FieldMaterial}
                    validate={errors.estado && touched.estado}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={procesoSeleccionada && procesoSeleccionada.estado}
                  />
                  {errors.estado && touched.estado && (
                    <div className={styles.ErrorMessage}> {errors.estado} </div>
                  )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="tipo_servicio">Tipo de servico</label>
                  <Field
                    component="select"
                    id="tipo_servicio"
                    name="tipo_servicio"
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder="--- Seleccione institucion---"
                    validate={errors.tipo_servicio && touched.tipo_servicio}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={
                      procesoSeleccionada && procesoSeleccionada.tipo_servicio
                    }
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione el tipo de servicio ---
                    </option>
                    <option
                      key="Estado"
                      value="Estado"
                      className={styles.FieldMaterial}
                    >
                      Estado
                    </option>
                    <option
                      key="Obra"
                      value="Obra"
                      className={styles.FieldMaterial}
                    >
                      Obra
                    </option>
                    <option
                      key="Servicio"
                      value="Servicio"
                      className={styles.FieldMaterial}
                    >
                      Servicio
                    </option>
                  </Field>
                  {errors.nombre_institucion && touched.nombre_institucion && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.nombre_institucion}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="objeto">Objeto</label>
                  <Field
                    type="text"
                    id="objeto"
                    name="objeto"
                    className={styles.FieldMaterial}
                    validate={errors.objeto && touched.objeto}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={procesoSeleccionada && procesoSeleccionada.objeto}
                  />
                  {errors.objeto && touched.objeto && (
                    <div className={styles.ErrorMessage}> {errors.objeto} </div>
                  )}
                </FormGroup>
              </Grid>
              <Grid item xs={6} md={6}>
                <br />
                <FormGroup>
                  <label htmlFor="informacion_adicional">
                    Informacion Adicional
                  </label>
                  <Field
                    component="textarea"
                    id="informacion_adicional"
                    name="informacion_adicional"
                    className={styles.FieldMaterial}
                    validate={
                      errors.informacion_adicional &&
                      touched.informacion_adicional
                    }
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={
                      procesoSeleccionada &&
                      procesoSeleccionada.informacion_adicional
                    }
                  />
                  {errors.informacion_adicional &&
                    touched.informacion_adicional && (
                      <div className={styles.ErrorMessage}>
                        {" "}
                        {errors.informacion_adicional}{" "}
                      </div>
                    )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="indicaciones_generales">
                    Indicaciones Generales
                  </label>
                  <Field
                    component="textarea"
                    id="indicaciones_generales"
                    name="indicaciones_generales"
                    className={styles.FieldMaterial}
                    validate={
                      errors.indicaciones_generales &&
                      touched.indicaciones_generales
                    }
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={
                      procesoSeleccionada &&
                      procesoSeleccionada.indicaciones_generales
                    }
                  />
                  {errors.indicaciones_generales &&
                    touched.indicaciones_generales && (
                      <div className={styles.ErrorMessage}>
                        {" "}
                        {errors.indicaciones_generales}{" "}
                      </div>
                    )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="nombre_institucion">
                    Seleccion el nombre de institucion
                  </label>
                  <Field
                    component="select"
                    id="nombre_institucion"
                    name="nombre_institucion"
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder="--- Seleccione institucion---"
                    validate={
                      errors.nombre_institucion && touched.nombre_institucion
                    }
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={
                      procesoSeleccionada &&
                      procesoSeleccionada.nombre_institucion
                    }
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione institucion ---
                    </option>
                    {institucion.map((institucion) => (
                      <option
                        key={institucion.nombre}
                        value={institucion.nombre}
                        className={styles.FieldMaterial}
                      >
                        {institucion.nombre}
                      </option>
                    ))}
                  </Field>
                  {errors.nombre_institucion && touched.nombre_institucion && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.nombre_institucion}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <FormGroup>
                  <label htmlFor="nombre_uaci">
                    Seleccion el nombre de uaci
                  </label>
                  <Field
                    component="select"
                    id="nombre_uaci"
                    name="nombre_auci"
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder="--- Seleccione uaci---"
                    validate={errors.nombre_uaci && touched.nombre_uaci}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={
                      procesoSeleccionada && procesoSeleccionada.nombre_uaci
                    }
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione uaci ---
                    </option>
                    {uaci.map((uaci) => (
                      <option
                        key={uaci.nombre}
                        value={uaci.nombre}
                        className={styles.FieldMaterial}
                      >
                        {uaci.nombre}
                      </option>
                    ))}
                  </Field>
                  {errors.nombre_uaci && touched.nombre_uaci && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.nombre_uaci}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <br />
                <Stack direction="row" justifyContent="center" spacing={2}>
                  <Button
                    variant="contained"
                    color="success"
                    onClick={() => peticionPut()}
                  >
                    Insertar
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={() => abrirCerrarModalEditar()}
                  >
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );
  //modal para eliminar un proceso
  const bodyEliminar = (
    <div className={styles.modal}>
      <p>
        Estás seguro que deseas eliminar el proceso{" "}
        <b>{procesoSeleccionada && procesoSeleccionada.nombre}</b>?{" "}
      </p>
      <div align="right">
        <Button color="secondary" onClick={() => peticionDelete()}>
          Sí
        </Button>
        <Button onClick={() => abrirCerrarModalEliminar()}>No</Button>
      </div>
    </div>
  );
  return (
    <>
      <div className={table.table}>
        <MaterialTable
          columns={columns}
          data={data}
          title="Info de Procesos"
          //Propiedades de los iconos eliminar y editar
          actions={[
            {
              icon: () => <AddIcon />,
              tooltip: "Agregar proceso",
              isFreeAction: true,
              onClick: (event, rowData) => abrirCerrarModalInsertar(),
            },
            {
              icon: () => <EditIcon />,
              tooltip: "Editar proceso",
              onClick: (event, rowData) =>
                seleccionarProceso(rowData, "Editar"),
            },
            {
              icon: () => <DeleteIcon />,
              tooltip: "Eliminar proceso",
              onClick: (event, rowData) =>
                seleccionarProceso(rowData, "Eliminar"),
            },
            {
              icon: () => <PictureAsPdfIcon />, // you can pass icon too
              tooltip: "Export to Pdf",
              onClick: () => downloadPdf(data),
              isFreeAction: true,
            },
          ]}
          //Propiedades de la tabla
          options={{
            filtering: true,
            grouping: true,
            sorting: true,
            thirdSortClick: false,
            search: true,
            searchFieldAlignment: "right",
            searchAutoFocus: true,
            searchFieldVariant: "standard",
            paging: true,
            pageSizeOptions: [5, 10, 20, 25, 50, 100],
            pageSize: 5,
            paginationType: "stepped",
            showFirstLastPageButtons: false,
            paginationPosition: "bottom",
            addRowPosition: "first",
            actionsColumnIndex: -1,
            columnsButton: true,
          }}
          localization={{
            header: {
              actions: "Acciones",
            },
          }}
        />
        <Modal open={modalInsertar} onClose={abrirCerrarModalInsertar}>
          {bodyInsertar}
        </Modal>

        <Modal open={modalEditar} onClose={abrirCerrarModalEditar}>
          {bodyEditar}
        </Modal>

        <Modal open={modalEliminar} onClose={abrirCerrarModalEliminar}>
          {bodyEliminar}
        </Modal>
      </div>
    </>
  );
}

export default Procesos;
