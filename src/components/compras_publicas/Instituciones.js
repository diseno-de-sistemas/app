import React, { useState, useEffect } from 'react'
import axios from "axios";
import MaterialTable from '@material-table/core';
import { Formik, Form, Field } from "formik";
import { Modal} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import "../Home.css";
import { FormGroup} from "@mui/material";
import jsPDF from "jspdf";
import "jspdf-autotable";
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
//Columnas de la tabla
const columns = [
    {
      title: "Nombre",
      field: "nombre",
      grouping: true,
      
    },
    {
      title: "Direccion",
      field: "direccion",
      grouping: false,
      sorting: false,
      filtering: false,
    },
    {
      title: "Telefono",
      field: "telefono",
      grouping: false,
      sorting: false,
    },
  ];

  const downloadPdf = (dat) => {
    const doc = new jsPDF()
    doc.text("Instituciones", 20, 10)
    doc.autoTable({
      theme: "grid",
      columns: columns.map(col => ({ ...col, dataKey: col.field })),
      body: dat
    })
    doc.save('Instituciones.pdf')
  }

//url de la api
const baseUrl = "http://45.56.114.13:8000/api/cp/instituciones/";

//Estilos del modal
const useStyles = makeStyles((theme) => ({
    modal: {
      position: "absolute",
      width: 500,
      backgroundColor: theme.palette.background.paper,
      border: "2px solid #000",
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
    },
    iconos: {
      cursor: "pointer",
    },
    FieldMaterial: {
      width: "100%",
      
    },
    ErrorMessage: {
      color:'#FF0000',
      fontStyle: "italic",
      fontFamily: "Times New Roman",
      fontSize: "15px",
      
    },
  }));
  
  //Estilos para la DataTable
  const tableStyles = makeStyles(theme => ({
    table: {
        marginTop: theme.spacing(3),
        '& thead th': {
            fontWeight: '600',
            color: theme.palette.primary.main,
            backgroundColor: " #d5d6dc ",
        },
        '& tbody td': {
            fontWeight: '300',
        },
        '& tbody tr:hover': {
            backgroundColor: '#fffbf2',
            cursor: 'pointer',
        },
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        borderBlockStyle: 'solid',
        borderBlockWidth: 'medium',
    },
  }))
  
function Instituciones() {
    const styles = useStyles();
    const table = tableStyles();
    const [data, setData] = useState([]);
    const [modalInsertar, setModalInsertar] = useState(false);
    const [modalEditar, setModalEditar] = useState(false);
    const [modalEliminar, setModalEliminar] = useState(false);
    const [institucionSeleccionada, setInstitucionSeleccionada] = useState({
        id: "",
        nombre: "",
        direccion: "",
        telefono: "",
      });
    
    const handleChange = (e) => {
        const { name, value } = e.target;
        setInstitucionSeleccionada((prevState) => ({
        ...prevState,
        [name]: value,
        }));
    }; 
   
    //Trae los datos desde la api
    const peticionGet = async () => {
        await axios
            .get(baseUrl)
            .then((response) => {
                setData(response.data);
            })
            .catch((error) => {
                console.log(error);
        });
    };
    
    const peticionPost = async (values) => {
      const headers = { "Content-Type": "application/json" };
        await axios
          .post(baseUrl,{nombre: values.nombre, direccion: values.direccion, telefono: values.telefono} ,headers)
          .then((response) => {
            setData(data.concat(response.data));
            console.log(institucionSeleccionada);
            abrirCerrarModalInsertar();
          })
          .catch((error) => {
            console.log(error);
        });
    };

    const peticionPut = async (values) => {
      const headers = { "Content-Type": "application/json" };
      await axios
        .put(baseUrl + "modificar/" + institucionSeleccionada.id + "/", institucionSeleccionada,headers)
        .then((response) => {
          var dataNueva = data;
          dataNueva.map((institucion) => {
            if (institucion.id === institucionSeleccionada.id) {
              institucion.nombre = institucionSeleccionada.nombre;
              institucion.direccion = institucionSeleccionada.direccion;
              institucion.telefono = institucionSeleccionada.telefono;             
            }
          });
          setData(dataNueva);
          abrirCerrarModalEditar();
        })
        .catch((error) => {
          console.log(error);
        });
    };
    const peticionDelete = async () => {
      await axios
        .delete(baseUrl + "modificar/" + institucionSeleccionada.id + "/")
        .then((response) => {
          setData(
            data.filter((institucion) => institucion.id !== institucionSeleccionada.id)
          );
          abrirCerrarModalEliminar();
        })
        .catch((error) => {
          console.log(error);
        });
    };

    const seleccionarInstitucion = (institucion, caso) => {
      setInstitucionSeleccionada(institucion);
      caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
    };
  
    const abrirCerrarModalInsertar = () => {
      setModalInsertar(!modalInsertar);
    };
  
    const abrirCerrarModalEditar = () => {
      setModalEditar(!modalEditar);
    };
  
    const abrirCerrarModalEliminar = () => {
      setModalEliminar(!modalEliminar);
    };
  
    useEffect(() => {
      peticionGet();
    }, []);
    
    useEffect(() => {
      if (modalInsertar === false || modalEditar === false)
        peticionGet();
    }, [modalInsertar, modalEditar,modalEliminar]);

    //Modal para insertar Institucion
    const [formularioEnviado, cambiarFormularioEnviado] = useState(false);
    const bodyInsertar = (
    <div className={styles.modal}>
      <h3>Nueva Institucion</h3>
      <br/>
      <Formik
				initialValues={{
          nombre: "",
          direccion: "",
          telefono: "",
				}}
				validate={(values) => {
					const errors = {};
          
          // Validacion de telefono, Solo para telefonos de ES
					if(!values.telefono){
						errors.telefono = 'Por favor ingresa el numero de telefono'
					} else if(!/^[1-9][0-9]{3}-[0-9]{4}$/.test(values.telefono)){
						errors.telefono = 'El numero de telefono debe de contener el siguiente formato:####-####'
					}

          //Validaciones para evitar campos en blanco a la hora de enviar el formulario
          if(!values.direccion){
						errors.direccion = 'Por favor ingresar la direccion'
          }
          if(!values.nombre){
						errors.nombre = 'Por favor ingresar la especializacion'
          }
					return errors;
          }}
          onSubmit={(values, {resetForm}) => {
            peticionPost();
            resetForm();
            console.log('Formulario enviado');
            cambiarFormularioEnviado(true);
            setTimeout(() => cambiarFormularioEnviado(false), 5000);
          }}
			  >
          {( {errors,touched,handleBlur,handleChange,handleSubmit,values } ) => (
					<Form >
            
                <FormGroup>
                  <label htmlFor="nombre">Nombre</label>
                  <Field
                        type="text" 
                        id="nombre" 
                        name="nombre" 
                        className={styles.FieldMaterial}
                        validate={errors.nombre && touched.nombre}
                        onChange={handleChange}
                        onBlur={handleBlur}      
                  />
                  { errors.nombre&& touched.nombre && < div className={styles.ErrorMessage} > { errors.nombre } </ div > } 
                </FormGroup>
                <br/>
                <FormGroup>
                  <label htmlFor="direccion">Direccion</label>
                  <Field
                      type="text" 
                      id="direccion" 
                      name="direccion" 
                      placeholder="col.Los lirios, pje 7 ote poligono o block j, san salvador"
                      className={styles.FieldMaterial}
                      validate={errors.direccion && touched.direccion}
                      onChange={handleChange}
                      onBlur={handleBlur}
                     
                  />
                  { errors.direccion && touched.direccion && < div className={styles.ErrorMessage} > { errors.direccion } </ div > } 
                </FormGroup>
                <br/>
                <FormGroup>
                  <label htmlFor="telefono">Telefono</label>
                  <Field
                      type="text" 
                      id="telefono" 
                      name="telefono" 
                      placeholder="2222-2222"
                      className={styles.FieldMaterial}
                      validate={errors.telefono && touched.telefono}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      
                  />
                  { errors.telefono && touched.telefono && < div className={styles.ErrorMessage} > { errors.telefono } </ div > }
                </FormGroup>
                <br/><br/>
                  <Stack direction="row" justifyContent='center' spacing={2}>
                    <Button variant="contained"  color="success" onClick={() => peticionPut(values)}>Insertar</Button>
                    <Button variant="contained"  color="error" onClick={() => abrirCerrarModalInsertar()}>Cancelar</Button>
                  </Stack>                                                                       
              
          </Form>
        )}
      </Formik>
    </div>
  );

  //Modal para insertar Institucion
  //const [formularioEnviado, cambiarFormularioEnviado] = useState(false);
  const bodyEditar = (
  <div className={styles.modal}>
    <h3>Editar Institucion</h3>
    <br/>
    <Formik
      initialValues={{
        nombre: "",
        direccion: "",
        telefono: "",
      }}
      validate={(values) => {
        const errors = {};
        
        // Validacion de telefono, Solo para telefonos de ES
        if(!values.telefono){
          errors.telefono = 'Por favor ingresa el numero de telefono'
        } else if(!/^[1-9][0-9]{3}-[0-9]{4}$/.test(values.telefono)){
          errors.telefono = 'El numero de telefono debe de contener el siguiente formato:####-####'
        }

        //Validaciones para evitar campos en blanco a la hora de enviar el formulario
        if(!values.direccion){
          errors.direccion = 'Por favor ingresar la direccion'
        }
        if(!values.nombre){
          errors.nombre = 'Por favor ingresar la especializacion'
        }
        return errors;
        }}
        onSubmit={(values, {resetForm}) => {
          peticionPut();
          resetForm();
          console.log('Formulario enviado');
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {( {errors,touched,handleBlur,handleSubmit,values } ) => (
        <Form >
              <FormGroup>
                <label htmlFor="nombre">Nombre</label>
                <Field
                      type="text" 
                      id="nombre" 
                      name="nombre" 
                      className={styles.FieldMaterial}
                      validate={errors.nombre && touched.nombre}
                      onChange={handleChange}
                      onBlur={handleBlur} 
                      value={institucionSeleccionada && institucionSeleccionada.nombre}     
                />
                { errors.nombre&& touched.nombre && < div className={styles.ErrorMessage} > { errors.nombre } </ div > } 
              </FormGroup>
              <br/>
              <FormGroup>
                <label htmlFor="direccion">Direccion</label>
                <Field
                    type="text" 
                    id="direccion" 
                    name="direccion" 
                    placeholder="col.Los lirios, pje 7 ote poligono o block j, san salvador"
                    className={styles.FieldMaterial}
                    validate={errors.direccion && touched.direccion}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={institucionSeleccionada && institucionSeleccionada.direccion}
                />
                { errors.direccion && touched.direccion && < div className={styles.ErrorMessage} > { errors.direccion } </ div > } 
              </FormGroup>
              <br/>
              <FormGroup>
                <label htmlFor="telefono">Telefono</label>
                <Field
                    type="text" 
                    id="telefono" 
                    name="telefono" 
                    placeholder="2222-2222"
                    className={styles.FieldMaterial}
                    validate={errors.telefono && touched.telefono}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={institucionSeleccionada && institucionSeleccionada.telefono}
                />
                { errors.telefono && touched.telefono && < div className={styles.ErrorMessage} > { errors.telefono } </ div > }
              </FormGroup>                                                                                                                           
                <br/><br/>
                <Stack direction="row" justifyContent='center' spacing={2}>
                  <Button variant="contained" color="success" onClick={() => peticionPut(values)}>Editar</Button>
                  <Button variant="contained" color="error" onClick={() => abrirCerrarModalEditar()}>Cancelar</Button>
                </Stack>            
          
        </Form>
        )}
      </Formik>
    </div>
  );
  //modal para eliminar una empresa
  const bodyEliminar=(
    <div className={styles.modal}>
      <p>Estás seguro que deseas eliminar la institucion <b>{institucionSeleccionada && institucionSeleccionada.nombre}</b>? </p>
      <div align="right">
        <Button color="secondary" onClick={()=>peticionDelete()}>Sí</Button>
        <Button onClick={()=>abrirCerrarModalEliminar()}>No</Button>
      </div>
    </div>
  )
    return (
      <>
      <div className={table.table}>
        <MaterialTable
          columns={columns}
          data={data}
          title="Info de Instituciones"
          //Propiedades de los iconos eliminar y editar
          actions={[
            {
              icon: () => <AddIcon />,
              tooltip: 'Agregar institucion',
              isFreeAction: true,
              onClick: (event, rowData) => abrirCerrarModalInsertar(),
             
            },
            {
              icon: () => <EditIcon />,
              tooltip: 'Editar institucion',
              onClick: (event, rowData) => seleccionarInstitucion(rowData, "Editar")
            },
            {
              icon: () => <DeleteIcon />,
              tooltip: 'Eliminar institucion',
              onClick: (event, rowData) => seleccionarInstitucion(rowData, "Eliminar")
            },
            {
              icon: () => <PictureAsPdfIcon />,// you can pass icon too
              tooltip: "Export to Pdf",
              onClick: () => downloadPdf(data),
              isFreeAction: true
            },
          ]}
          //Propiedades de la tabla
          options={{
            
            filtering: true,
            grouping: true,
            sorting: true,
            thirdSortClick: false,
            search: true,
            searchFieldAlignment: "right", 
            searchAutoFocus: true, 
            searchFieldVariant: "standard",
            paging: true,
            pageSizeOptions: [5, 10, 20, 25, 50, 100],
            pageSize: 5,
            paginationType: "stepped",
            showFirstLastPageButtons: false,
            paginationPosition: "bottom",
            addRowPosition: "first",
            actionsColumnIndex: -1,
            columnsButton: true,
          }}
          localization={{
            header:{
              actions: "Acciones"
            }
          }}
          
        />
        <Modal
        open={modalInsertar}
        onClose={abrirCerrarModalInsertar}>
          {bodyInsertar}
        </Modal>

        <Modal
        open={modalEditar}
        onClose={abrirCerrarModalEditar}>
          {bodyEditar}
        </Modal>

        <Modal
        open={modalEliminar}
        onClose={abrirCerrarModalEliminar}>
          {bodyEliminar}
        </Modal>
      </div>

    </>
  );
}

export default Instituciones;