import React, { useState, useEffect } from "react";
import axios from "axios";
import MaterialTable from '@material-table/core';
import { Formik, Form, Field } from "formik";
import { Modal,  Grid, } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import "../Home.css";
import { FormGroup} from "@mui/material";
import jsPDF from "jspdf";
import "jspdf-autotable";
import AddIcon from '@mui/icons-material/Add';
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
//Columnas de la tabla
const columns = [
  {
    title: "Fecha",
    field: "fecha_adquisicion",
    type: "date",
    grouping: true,
  },
  {
    title: "RazonSocial",
    field: "razon_social",
    grouping: true,
    sorting: false,
  },
  {
    title: "Telefono",
    field: "telefono",
    grouping: false,
    sorting: false,
  },
  {
    title: "Fax",
    field: "telefono_fax",
    filtering: false,
    grouping: false,
    sorting: false,
  },
  {
    title: "Correo",
    field: "correo_electronico",
    grouping: false,
    sorting: false,
  },
  {
    title: "NIT",
    field: "nit",
    grouping: false,
    sorting: false,
  },
  {
    title: "Direccion",
    field: "direccion",
    filtering: false,
    grouping: false,
    sorting: false,
  },
  
  {
    title: "Especializacion",
    field: "especializacion",
    filtering: false,
    grouping: false,
    sorting: false,
  },
  {
    title: "InfoAdicional",
    field: "informacion_adicional",
    filtering: false,
    grouping: false,
    sorting: false, 
  },
];

const downloadPdf = (dat) => {
  const doc = new jsPDF()
  doc.text("Empresas", 20, 10)
  doc.autoTable({
    theme: "grid",
    columns: columns.map(col => ({ ...col, dataKey: col.field })),
    body: dat
  })
  doc.save('Empresas.pdf')
}

//url de la api
const baseUrl = "http://45.56.114.13:8000/api/cp/empresa/";

//Estilos del modal
const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 1000,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  FieldMaterial: {
    width: "100%",
    
  },
  ErrorMessage: {
    color:'#FF0000',
    fontStyle: "italic",
    fontFamily: "Times New Roman",
    fontSize: "15px",
    
  },
}));

//Estilos para la DataTable
const tableStyles = makeStyles(theme => ({
  table: {
      marginTop: theme.spacing(3),
      '& thead th': {
          fontWeight: '600',
          color: theme.palette.primary.main,
          backgroundColor: " #d5d6dc ",
      },
      '& tbody td': {
          fontWeight: '300',
          
      },
      '& tbody tr:hover': {
          backgroundColor: '#fffbf2',
          cursor: 'pointer',
      },
      marginLeft: theme.spacing(3),
      marginRight: theme.spacing(3),
      borderBlockStyle: 'solid',
      borderBlockWidth: 'medium',
  },
}))

function Empesa() {
  const styles = useStyles();
  const table = tableStyles();
  const [data, setData] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [empresaSeleccionada, setEmpresaSeleccionada] = useState({
    id: "",
    fecha_adquisicion: "",
    razon_social: "",
    telefono: "",
    correo_electronico: "",
    nit: "",
    direccion: "",
    telefono_fax: "",
    especializacion: "",
    informacion_adicional: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setEmpresaSeleccionada((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  //Trae los data desde la api
  const peticionGet = async () => {
    await axios
      .get(baseUrl)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  //const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;
  const peticionPost = async (values) => {
    const headers = { "Content-Type": "application/json" };
    await axios.post(baseUrl,
      {fecha_adquisicion:values.fecha_adquisicion,
       razon_social:values.razon_social,
       telefono:values.telefono,
       correo_electronico:values.correo_electronico,
       nit:values.nit,
       direccion:values.direccion,
       telefono_fax:values.telefono_fax,
       especializacion:values.especializacion,
       informacion_adicional: values.informacion_adicional},headers)
      .then((response) => {
        setData(data.concat(response.data));
        abrirCerrarModalInsertar();
      })
      .catch((error) => {
        console.log(error);
      });
  };
  //console.log(peticionPost);
  const peticionPut = async (values) => {
    const headers = { "Content-Type": "application/json" };
    await axios
      .put(baseUrl + "modificar/"  + empresaSeleccionada.id + "/", empresaSeleccionada,headers)
      .then((response) => {
        var dataNueva = data;
        dataNueva.map((empresa) => {
          if (empresa.id === empresaSeleccionada.id) {
            empresa.fecha_adquisicion = empresaSeleccionada.fecha_adquisicion;
            empresa.razon_social = empresaSeleccionada.razon_social;
            empresa.telefono = empresaSeleccionada.telefono;
            empresa.telefono_fax = empresaSeleccionada.telefono_fax;
            empresa.correo_electronico = empresaSeleccionada.correo_electronico;
            empresa.nit = empresaSeleccionada.nit;
            empresa.direccion = empresaSeleccionada.direccion;
            empresa.especializacion = empresaSeleccionada.especializacion;
            empresa.informacion_adicional = empresaSeleccionada.informacion_adicional;
          }
        });
        setData(dataNueva);
        abrirCerrarModalEditar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const peticionDelete = async () => {
    await axios
      .delete(baseUrl + "modificar/" + empresaSeleccionada.id + "/")
      .then((response) => {
        setData(
          data.filter((empresa) => empresa.id !== empresaSeleccionada.id)
        );
        abrirCerrarModalEliminar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const seleccionarEmpresa = (empresa, caso) => {
    setEmpresaSeleccionada(empresa);
    caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };

  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };

  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };

  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  useEffect(() => {
    peticionGet();
  }, []);

  useEffect(() => {
    if (modalInsertar === true || modalEditar === true)
      peticionGet();
  }, [modalInsertar, modalEditar,modalEliminar]);
  
  //Modal para insertar una empresa
  const [formularioEnviado, cambiarFormularioEnviado] = useState(false);
  const bodyInsertar = (
    <div className={styles.modal}>
      <h3>Agregar Empresa</h3>
      <br/>
      <Formik
				initialValues={{
          fecha_adquisicion: "",
          razon_social: "",
          telefono: "",
          correo_electronico: "",
          nit: "",
          direccion: "",
          telefono_fax: "",
          especializacion: "",
          informacion_adicional: "",
				}}
				validate={(values) => {
					const errors = {};

					// Validacion nombreRazon
					if(!values.razon_social){
						errors.razon_social = 'Por favor ingresa una Razon'
					}
          
          // Validacion de telefono, Solo para telefonos de ES
					if(!values.telefono){
						errors.telefono = 'Por favor ingresa el numero de telefono'
					} else if(!/^[1-9][0-9]{3}-[0-9]{4}$/.test(values.telefono)){
						errors.telefono = 'El numero de telefono debe de contener el siguiente formato:####-####'
					}

          // Validacion fax, Solo para fax de ES
					if(!values.telefono_fax){
						errors.telefono_fax = 'Por favor ingresa el numero de fax'
					} else if(!/^[1-9][0-9]{3}-[0-9]{4}$/.test(values.telefono_fax)){
						errors.telefono_fax = 'El numero de fax debe de contener el siguiente formato:####-####'
					}

            

          // Validacion del NIT
					if(!values.nit){
						errors.nit = 'Por favor ingresa el NIT'
					} else if(!/^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}$/.test(values.nit)){
						errors.nit = 'El NIT debe  de contener el siguiente formato:####-######-###-#'
					}

					// Validacion correo
					if(!values.correo_electronico){
						errors.correo_electronico = 'Por favor ingresa un correo electronico'
					} else if(!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(values.correo_electronico)){
						errors.correo_electronico = 'El correo solo puede contener letras, numeros, puntos, guiones y guion bajo.'
					}

          //Validaciones para evitar campos en blanco a la hora de enviar el formulario
          if(!values.direccion){
						errors.direccion = 'Por favor ingresar la direccion'
          }
          if(!values.especializacion){
						errors.especializacion = 'Por favor ingresar la especializacion'
          }
          if(!values.informacion_adicional){
						errors.informacion_adicional = 'Por favor ingresar la informacion adicional'
          }
          // Validacion de fecha
					if(!values.fecha_adquisicion){
						errors.fecha_adquisicion = 'Por favor ingresa la fecha'
					}

					return errors;
          }}
          onSubmit={(values, {resetForm}) => {
            peticionPost();
            resetForm();
            console.log('Formulario enviado');
            cambiarFormularioEnviado(true);
            setTimeout(() => cambiarFormularioEnviado(false), 5000);
          }}
			  >
          {( {errors,touched,handleBlur,handleChange,handleSubmit,values } ) => (
					<Form >
            <Grid container  spacing={3} >
              <Grid  item xs={6} >
                <FormGroup>
                  <label htmlFor="fecha_adquisicion">Fecha</label>
                  <Field
                        type="date" 
                        id="fecha_adquisicion" 
                        name="fecha_adquisicion" 
                        placeholder="7/11/2021"
                        className={styles.FieldMaterial}
                        validate={errors.fecha_adquisicion && touched.fecha_adquisicion}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        
                  />
                  { errors.fecha_adquisicion&& touched.fecha_adquisicion && < div className={styles.ErrorMessage} > { errors.fecha_adquisicion } </ div > } 
                </FormGroup>
        
                <FormGroup>
                  <label htmlFor="razon_social">Razon Social</label>
                  <Field
                      type="text" 
                      id="razon_social" 
                      name="razon_social" 
                      placeholder="Razon social"
                      className={styles.FieldMaterial}
                      validate={errors.razon_social && touched.razon_social}
                      onChange={handleChange}
                      onBlur={handleBlur}
                     
                  />
                  { errors.razon_social && touched.razon_social && < div className={styles.ErrorMessage} > { errors.razon_social } </ div > }
                  
                </FormGroup>
                
                <FormGroup>
                  <label htmlFor="telefono">Telefono</label>
                  <Field
                      type="text" 
                      id="telefono" 
                      name="telefono" 
                      placeholder="2222-2222"
                      className={styles.FieldMaterial}
                      validate={errors.telefono && touched.telefono}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      
                  />
                  { errors.telefono && touched.telefono && < div className={styles.ErrorMessage} > { errors.telefono } </ div > }
                </FormGroup>                
                
                <FormGroup>
                  <label htmlFor="telefono_fax">fax</label>
                  <Field
                      type="text" 
                      id="telefono_fax" 
                      name="telefono_fax" 
                      placeholder="2222-2222"
                      className={styles.FieldMaterial}
                      validate={errors.telefono_fax && touched.telefono_fax}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      
                  />
                  { errors.telefono_fax && touched.telefono_fax && < div className={styles.ErrorMessage} > { errors.telefono_fax} </ div > }
                </FormGroup>               
                
                <FormGroup>
                  <label htmlFor="correo_electronico">Correo</label>
                  <Field
                      type="email" 
                      id="correo_electronico" 
                      name="correo_electronico" 
                      placeholder="correo@correo.com" 
                      className={styles.FieldMaterial}
                      validate={errors.correo_electronico && touched.correo_electronico}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      
                  />
                  { errors.correo_electronico && touched.correo_electronico && < div className={styles.ErrorMessage} > { errors.correo_electronico} </ div > }
                </FormGroup>               
                
                                         
              </Grid>            
              <Grid  item xs={6} md={6} >
                <FormGroup>
                    <label htmlFor="nit">NIT</label>
                    <Field
                        type="text" 
                        id="nit" 
                        name="nit" 
                        placeholder="0528-190732-118-0"
                        className={styles.FieldMaterial}
                        validate={errors.nit && touched.nit}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        
                    />
                  { errors.nit && touched.nit && < div className={styles.ErrorMessage} > { errors.nit } </ div > }
                </FormGroup> 
                <FormGroup>
                  <label htmlFor="direccion">Direccion</label>
                      <Field
                        type="text" 
                        id="direccion" 
                        name="direccion" 
                        placeholder="col.Los lirios, pje 7 ote poligono o block j, san salvador"
                        className={styles.FieldMaterial}
                        validate={errors.direccion && touched.direccion}
                        onChange={handleChange}
                        onBlur={handleBlur}                       
                      />
                      { errors.direccion && touched.direccion && < div className={styles.ErrorMessage} > { errors.direccion } </ div > }
                </FormGroup> 
                <FormGroup>
                  <label htmlFor="especializacion">Especializacion</label>
                          <Field
                            type="textarea" 
                            id="especializacion" 
                            name="especializacion"
                            className={styles.FieldMaterial}
                            validate={errors.especializacion && touched.especializacion}
                            onChange={handleChange}
                            onBlur={handleBlur}                           
                        />
                        { errors.especializacion && touched.especializacion && < div className={styles.ErrorMessage} > { errors.especializacion } </ div > }
                </FormGroup>    
                
                <FormGroup>
                  <label htmlFor="informacion_adicional">Info Adicional</label>
                      <Field
                        component='textarea' 
                        id="informacion_adicional" 
                        name="informacion_adicional"
                        className={styles.FieldMaterial}
                        validate={errors.informacion_adicional && touched.informacion_adicional}
                        onChange={handleChange}
                        onBlur={handleBlur}                         
                      />
                      { errors.informacion_adicional && touched.informacion_adicional && < div className={styles.ErrorMessage} > { errors.informacion_adicional } </ div > }
                </FormGroup>                  
                  <br/><br/>
                  <Stack direction="row" spacing={2}>
                    <Button variant='contained' color="success" onClick={() => peticionPost(values)}>Insertar</Button>
                    <Button variant='contained' color="error" onClick={() => abrirCerrarModalInsertar()}>Cancelar</Button>
                  </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );
  
  //Modal para editar una empresa
  const bodyEditar = (
    <div className={styles.modal}>
      <h3>Editar Empresa</h3>
      <Formik
				validate={(values) => {
					const errors = {};

					// Validacion nombreRazon
					if(!values.razon_social){
						errors.razon_social = 'Por favor ingresa una Razon'
					} 
					
          
          // Validacion de telefono, Solo para telefonos de ES
					if(!values.telefono){
						errors.telefono = 'Por favor ingresa el numero de telefono'
					} else if(!/^[1-9][0-9]{3}-[0-9]{4}$/.test(values.telefono)){
						errors.telefono = 'El numero de telefono debe de contener el siguiente formato:####-####'
					}

          // Validacion fax, Solo para fax de ES
					if(!values.telefono_fax){
						errors.telefono_fax = 'Por favor ingresa el numero de fax'
					} else if(!/^[1-9][0-9]{3}-[0-9]{4}$/.test(values.telefono_fax)){
						errors.telefono_fax = 'El numero de fax debe de contener el siguiente formato:####-####'
					}

           // Validacion de fecha
					if(!values.fecha_adquisicion){
						errors.fecha_adquisicion = 'Por favor ingresa la fecha'
					} else if(!/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[1-9]|2[1-9])$/.test(values.fecha_adquisicion)){
						errors.fecha_adquisicion = 'La fecha debe  de contener el siguiente formato:##/##/##'
					}

          // Validacion del NIT
					if(!values.nit){
						errors.nit = 'Por favor ingresa el NIT'
					} else if(!/^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}$/.test(values.nit)){
						errors.nit = 'El NIT debe  de contener el siguiente formato:####-######-###-#'
					}

					// Validacion correo
					if(!values.correo_electronico){
						errors.correo_electronico = 'Por favor ingresa un correo electronico'
					} else if(!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(values.correo_electronico)){
						errors.correo_electronico = 'El correo solo puede contener letras, numeros, puntos, guiones y guion bajo.'
					}

          //Validaciones para evitar campos en blanco a la hora de enviar el formulario
          if(!values.direccion){
						errors.direccion = 'Por favor ingresar la direccion'
          }
          if(!values.especializacion){
						errors.especializacion = 'Por favor ingresar la especializacion'
          }
          if(!values.informacion_adicional){
						errors.informacion_adicional = 'Por favor ingresar la informacion adicional'
          }

					return errors;
          }}
          onSubmit={(values, {resetForm}) => {
            peticionPut();
            resetForm();
            console.log('Formulario enviado');
            cambiarFormularioEnviado(true);
            setTimeout(() => cambiarFormularioEnviado(false), 5000);
          }}
			  >
          {( {errors,touched,handleBlur,handleSubmit,values } ) => (
					<Form  onSubmit={handleSubmit}>
            <Grid container  spacing={3} >
              <Grid  item xs={6} >
                <FormGroup>
                  <label htmlFor="fecha_adquisicion">Fecha</label>
                  <Field
                        type="date" 
                        id="fecha_adquisicion" 
                        name="fecha_adquisicion" 
                        placeholder="7/11/2021"
                        className={styles.FieldMaterial}
                        validate={errors.fecha_adquisicion && touched.fecha_adquisicion}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={empresaSeleccionada && empresaSeleccionada.fecha_adquisicion}
                  />
                  { errors.fecha_adquisicion&& touched.fecha_adquisicion && < div className={styles.ErrorMessage} > { errors.fecha_adquisicion } </ div > } 
                </FormGroup>
        
                <FormGroup>
                  <label htmlFor="razon_social">Razon Social</label>
                  <Field
                      type="text" 
                      id="razon_social" 
                      name="razon_social" 
                      placeholder="Razon social"
                      className={styles.FieldMaterial}
                      validate={errors.razon_social && touched.razon_social}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={empresaSeleccionada && empresaSeleccionada.razon_social}
                  />
                  { errors.razon_social && touched.razon_social && < div className={styles.ErrorMessage} > { errors.razon_social } </ div > }
                  
                </FormGroup>
                
                <FormGroup>
                  <label htmlFor="telefono">Telefono</label>
                  <Field
                      type="text" 
                      id="telefono" 
                      name="telefono" 
                      placeholder="2222-2222"
                      className={styles.FieldMaterial}
                      validate={errors.telefono && touched.telefono}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={empresaSeleccionada && empresaSeleccionada.telefono}
                  />
                  { errors.telefono && touched.telefono && < div className={styles.ErrorMessage} > { errors.telefono } </ div > }
                </FormGroup>                
                
                <FormGroup>
                  <label htmlFor="telefono_fax">fax</label>
                  <Field
                      type="telefono_fax" 
                      id="telefono_fax" 
                      name="telefono_fax" 
                      placeholder="2222-2222"
                      className={styles.FieldMaterial}
                      validate={errors.telefono_fax && touched.telefono_fax}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={empresaSeleccionada && empresaSeleccionada.telefono_fax}
                  />
                  { errors.telefono_fax && touched.telefono_fax && < div className={styles.ErrorMessage} > { errors.telefono_fax} </ div > }
                </FormGroup>               
                
                <FormGroup>
                  <label htmlFor="correo_electronico">Correo</label>
                  <Field
                      type="email" 
                      id="correo_electronico" 
                      name="correo_electronico" 
                      placeholder="correo@correo.com" 
                      className={styles.FieldMaterial}
                      validate={errors.correo_electronico && touched.correo_electronico}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={empresaSeleccionada && empresaSeleccionada.correo_electronico}
                  />
                  { errors.correo_electronico && touched.correo_electronico && < div className={styles.ErrorMessage} > { errors.correo_electronico} </ div > }
                </FormGroup>               
                
                                         
              </Grid>            
              <Grid  item xs={6} md={6} >
                <FormGroup>
                    <label htmlFor="nit">NIT</label>
                    <Field
                        type="text" 
                        id="nit" 
                        name="nit" 
                        placeholder="0528-190732-118-0"
                        className={styles.FieldMaterial}
                        validate={errors.nit && touched.nit}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={empresaSeleccionada && empresaSeleccionada.nit}
                    />
                  { errors.nit && touched.nit && < div className={styles.ErrorMessage} > { errors.nit } </ div > }
                </FormGroup> 
                <FormGroup>
                  <label htmlFor="direccion">Direccion</label>
                      <Field
                        type="text" 
                        id="direccion" 
                        name="direccion" 
                        placeholder="col.Los lirios, pje 7 ote poligono o block j, san salvador"
                        className={styles.FieldMaterial}
                        validate={errors.direccion && touched.direccion}
                        onChange={handleChange}
                        onBlur={handleBlur} 
                        value={empresaSeleccionada && empresaSeleccionada.direccion}                      
                      />
                      { errors.direccion && touched.direccion && < div className={styles.ErrorMessage} > { errors.direccion } </ div > }
                </FormGroup> 
                <FormGroup>
                  <label htmlFor="especializacion">Especializacion</label>
                          <Field
                            type="text" 
                            id="especializacion" 
                            name="especializacion"
                            className={styles.FieldMaterial}
                            validate={errors.especializacion && touched.especializacion}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={empresaSeleccionada && empresaSeleccionada.especializacion}                           
                        />
                        { errors.especializacion && touched.especializacion && < div className={styles.ErrorMessage} > { errors.especializacion } </ div > }
                </FormGroup>    
                
                <FormGroup>
                  <label htmlFor="informacion_adicional">Info Adicional</label>
                      <Field
                        component='textarea' 
                        id="informacion_adicional" 
                        name="informacion_adicional"
                        className={styles.FieldMaterial}
                        validate={errors.informacion_adicional && touched.informacion_adicional}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={empresaSeleccionada && empresaSeleccionada.informacion_adicional}                         
                      />
                      { errors.informacion_adicional && touched.informacion_adicional && < div className={styles.ErrorMessage} > { errors.informacion_adicional } </ div > }
                </FormGroup>                  
                  <br/><br/>
                  <Stack direction="row" spacing={2}>
                    <Button variant='contained' color="success"  onClick={()=>peticionPut(values)}>Editar</Button>
                    <Button variant="contained" color="error" onClick={() => abrirCerrarModalEditar()}>Cancelar</Button>
                  </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );
  
  //modal para eliminar una empresa
  const bodyEliminar=(
    <div className={styles.modal}>
      <p>Estás seguro que deseas eliminar la empresa <b>{empresaSeleccionada && empresaSeleccionada.razon_social}</b>? </p>
      <div align="right">
        <Button color="secondary" onClick={()=>peticionDelete()}>Sí</Button>
        <Button onClick={()=>abrirCerrarModalEliminar()}>No</Button>
      </div>
    </div>
  )

  return (
    <>
      <div className={table.table}>
        <MaterialTable
          columns={columns}
          data={data}
          title="Info de Empresas"
          //Propiedades de los iconos eliminar y editar
          actions={[
            {
              icon:()=> <AddIcon/>,
              tooltip: 'Agregar empresa',
              isFreeAction: true,
              onClick: (event, rowData) => abrirCerrarModalInsertar(),
             
            },
            {
              icon: ()=> <EditIcon/>,
              tooltip: 'Editar Empresa',
              onClick: (event, rowData) => seleccionarEmpresa(rowData, "Editar")
            },
            {
              icon: ()=> <DeleteIcon/>,
              tooltip: 'Eliminar Empresa',
              onClick: (event, rowData) => seleccionarEmpresa(rowData, "Eliminar")
            },
            {
              icon: () => <PictureAsPdfIcon />,// you can pass icon too
              tooltip: "Export to Pdf",
              onClick: () => downloadPdf(data),
              isFreeAction: true
            },
          ]}
          //Propiedades de la tabla
          options={{
            filtering: true,
            grouping: true,
            sorting: true,
            thirdSortClick: false,
            search: true,
            searchFieldAlignment: "right", 
            searchAutoFocus: true, 
            searchFieldVariant: "standard",
            paging: true,
            pageSizeOptions: [5, 10, 20, 25, 50, 100],
            pageSize: 5,
            paginationType: "stepped",
            showFirstLastPageButtons: false,
            paginationPosition: "bottom",
            addRowPosition: "first",
            actionsColumnIndex: -1,
            columnsButton: true,
          }}
          localization={{
            header:{
              actions: "Acciones"
            }
          }}
          
        />
        <Modal
        open={modalInsertar}
        onClose={abrirCerrarModalInsertar}>
          {bodyInsertar}
        </Modal>

        <Modal
        open={modalEditar}
        onClose={abrirCerrarModalEditar}>
          {bodyEditar}
        </Modal>

        <Modal
        open={modalEliminar}
        onClose={abrirCerrarModalEliminar}>
          {bodyEliminar}
        </Modal>
      </div>

    </>
  );
}
export default Empesa;
