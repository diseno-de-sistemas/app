import React, { useState } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { SidebarData } from "./SidebarData";
import SubMenu from "./SubMenu";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import { IconContext } from "react-icons/lib";
import { Grid, AppBar, Toolbar, makeStyles} from "@material-ui/core";
import LoginIcon from '@mui/icons-material/Login';
import Typography from '@mui/material/Typography';

/*const Nav = styled.div`
  background: #15171c;
  height: 80px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
*/
const NavIcon = styled(Link)`
  margin-left: 2rem;
  font-size: 2rem;
  height: 80px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  text-decoration: none;
`;

const SidebarNav = styled.nav`
  background: #15171c;
  width: 250px;
  height: 100vh;
  display: flex;
  justify-content: center;
  position: fixed;
  top: 0;
  left: ${({ sidebar }) => (sidebar ? "0" : "-100%")};
  transition: 350ms;
  z-index: 10;
`;

const SidebarWrap = styled.div`
  width: 100%;
`;

const useStyles = makeStyles(theme => ({
  root: {
      backgroundColor: '#15171c',
      
  },
}))

const Sidebar = () => {
  const [sidebar, setSidebar] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);

  const classes = useStyles();

  return (
    <>
      <AppBar position="static" className={classes.root}>
        <Toolbar>
          <Grid container alignItems="center">
            <IconContext.Provider value={{ color: "#fff" }}>
                <Grid item>
                  <NavIcon to="#">
                    <FaIcons.FaBars onClick={showSidebar} />
                  </NavIcon>
                </Grid>
                
                <Grid item sm ><Link to="/" style={{ color: 'inherit', textDecoration: 'inherit'}}> <Typography variant="h5" marginLeft='40px'  sx={{ flexGrow: 1 }}>
                  GOBDATA
                </Typography></Link></Grid>
                <Grid item>
                  <NavIcon to="/login">
                    <LoginIcon fontSize='large' sx={{ color: 'white' }}/>
                  </NavIcon>
                </Grid>
              <SidebarNav sidebar={sidebar}>
                <SidebarWrap>
                  <NavIcon to="#">
                    <AiIcons.AiOutlineClose onClick={showSidebar} />
                  </NavIcon>
                  {SidebarData.map((item, index) => {
                    return (
                      <SubMenu item={item} key={index} onClick={showSidebar} />
                    );
                  })}
                </SidebarWrap>
              </SidebarNav>
            </IconContext.Provider>
          </Grid>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Sidebar;
