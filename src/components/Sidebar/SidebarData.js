import React from 'react';
import * as RiIcons from 'react-icons/ri';
import * as MdIcons from 'react-icons/md';

export const SidebarData = [
  {
    title: 'Control Legislativo',
    path: '/control_legislativo',
    icon: <MdIcons.MdOutlineAccountBalance/>,
    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />,

    subNav: [
      {
        title: 'Sesiones Plenarias',
        path: '/sesionesplenarias',
        icon: <MdIcons.MdAssignmentInd />
      },
      {
        title: 'Dictamenes',
        path: '/dictamenes',
        icon: <MdIcons.MdAssignment />
      },
      {
        title: 'Diputados',
        path: '/diputados',
        icon: <MdIcons.MdGroup/>
      },
      {
        title: 'Votaciones',
        path: '/votaciones',
        icon: <MdIcons.MdHowToVote/>
      },
      {
        title: 'Comisiones',
        path: '/comisiones',
        icon: <MdIcons.MdRecordVoiceOver/>
      }
    ]
  },
  {
    title: 'Proyectos',
    path: '/proyectos',
    icon: <MdIcons.MdOutlineDocumentScanner />,

    iconClosed: <RiIcons.RiArrowDownSFill />,
    iconOpened: <RiIcons.RiArrowUpSFill />,

    subNav: [
      {
        title: 'Empresa',
        path: '/empresa',
        icon: <MdIcons.MdBusiness />
      },
      {
        title: 'Licitaciones',
        path: '/licitaciones',
        icon: <MdIcons.MdLibraryBooks />
      },
      {
        title: 'Instituciones',
        path: '/instituciones',
        icon: <MdIcons.MdHolidayVillage />,
        cName: 'sub-nav'
      },
      {
        title: 'Procesos',
        path: '/procesos',
        icon: <MdIcons.MdAutorenew />,
        cName: 'sub-nav'
      },
      {
        title: 'Uaci',
        path: '/uaci',
        icon: <MdIcons.MdAttachMoney />
      }
    ]
  },
];