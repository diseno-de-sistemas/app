import React from "react";
import './Home.css';
//import {Comision,Dictamen,Diputado,SesionPlenaria,Votacion} from './control_legislativo';
import Comisiones from "./control_legislativo/Comisiones";
import Dictamenes from "./control_legislativo/Dictamenes";
import Diputados from "./control_legislativo/Diputados";
import SesionesPlenarias from "./control_legislativo/SesionesPlenarias";
import Votaciones from "./control_legislativo/Votaciones";
//import {Empresa,Instituciones,Licitaciones,Procesos,Uaci} from './compras_publicas';
import Empresa from "./compras_publicas/Empresa";
import Instituciones from "./compras_publicas/Instituciones";
import Licitaciones from "./compras_publicas/Licitaciones";
import Procesos from "./compras_publicas/Procesos";
import Uaci from "./compras_publicas/Uaci";
import Login from "./Login";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Sidebar from "./Sidebar/Sidebar";
function Home() {
    return(
      <>
          <Router>
             <Sidebar/> 
                <Routes>
                  <Route exact path="/"/>
                  <Route exact path="/control_legislativo"/>
                  <Route exact path="/proyectos"/>
                  <Route exact path="/sesionesplenarias" element={<SesionesPlenarias/>}/>
                  <Route exact path="/dictamenes" element={<Dictamenes/>}/>
                  <Route exact path="/diputados" element={<Diputados/>}/>
                  <Route exact path="/votaciones"  element={<Votaciones/>}/>
                  <Route exact path="/comisiones"  element={<Comisiones/>}/> 
                  <Route exact path="/instituciones" element={<Instituciones/>}/>
                  <Route exact path="/procesos"  element={<Procesos/>}/>
                  <Route exact path="/uaci"  element={<Uaci/>}/>
                  <Route exact path="/empresa" element={<Empresa/>}/>
                  <Route exact path="/licitaciones"  element={<Licitaciones/>}/>
                  <Route exact path="/login"  element={<Login/>}/>
                </Routes>
                <img alt="" src="../../public/background.svg"></img> 
            </Router>
            
      </>
    );
  };

export default Home;
