import '../Home.css';
import axios from 'axios';
import MaterialTable from '@material-table/core';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import { useState, useEffect } from 'react';
import { Formik, Form, Field } from 'formik';
import { Modal, Grid, Input } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { FormGroup } from '@mui/material';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import AddIcon from '@mui/icons-material/Add';
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

//Columnas de la tabla
const columns = [
  {
    title: 'Número',
    field: 'numero',
    filterComponent: () => <Input placeholder='custom' />,
    grouping: true,
  },
  {
    title: 'Tipo',
    field: 'tipo_dictamen',
    filterComponent: () => <Input placeholder='custom' />,
    grouping: false,
    sorting: false,
  },
  {
    title: 'Extracto',
    field: 'extracto',
    filterComponent: () => <Input placeholder='custom' />,
    grouping: true,
    sorting: false,
  },
  {
    title: 'Comisión',
    field: 'nombre_comision',
    filterComponent: () => <Input placeholder='custom' />,
    grouping: true,
    sorting: false,
  },
  {
    title: 'Número de sesión plenaria',
    field: 'sesion_plenaria_num',
    filterComponent: () => <Input placeholder='custom' />,
    grouping: true,
    sorting: false,
  },
  {
    title: 'Periodo de legislatura',
    field: 'periodo_legislatura_rango_fecha',
    filtering: false,
    grouping: false,
    sorting: false,
  },
];

const downloadPdf = (dat) => {
  const doc = new jsPDF();
  doc.text('Dictamenes', 20, 10);
  doc.autoTable({
    theme: 'grid',
    columns: columns.map((col) => ({ ...col, dataKey: col.field })),
    body: dat,
  });
  doc.save('Dictamenes.pdf');
};

const baseUrl = 'http://45.56.114.13:8000/api/ctl/dictamenes/';

//Estilos del modal
const useStyles = makeStyles((theme) => ({
  modal: {
    position: 'absolute',
    width: 1000,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
  iconos: {
    cursor: 'pointer',
  },
  FieldMaterial: {
    width: '100%',
  },
  ErrorMessage: {
    color: '#FF0000',
    fontStyle: 'italic',
    fontFamily: 'Times New Roman',
    fontSize: '15px',
  },
}));

//Estilos para la DataTable
const tableStyles = makeStyles((theme) => ({
  table: {
    marginTop: theme.spacing(3),
    '& thead th': {
      fontWeight: '600',
      color: theme.palette.primary.main,
      backgroundColor: ' #d5d6dc ',
    },
    '& tbody td': {
      fontWeight: '300',
    },
    '& tbody tr:hover': {
      backgroundColor: '#fffbf2',
      cursor: 'pointer',
    },
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderBlockStyle: 'solid',
    borderBlockWidth: 'medium',
  },
}));

const Dictamenes = () => {
  const styles = useStyles();
  const table = tableStyles();

  const [data, setData] = useState([]);
  const [comisiones, setComisiones] = useState([]);
  const [dictamenes, setDictamenes] = useState([]);
  const [sesionesPlenarias, setSesionesPlenarias] = useState([]);
  const [periodosLegislatura, setperiodosLegislatura] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);

  const [dictamenSeleccionado, setDictamenSeleccionado] = useState({
    id: '',
    numero: '',
    tipo_dictamen: '',
    extracto: '',
    nombre_comision: '',
    documento_link: '',
    sesion_plenaria_num: '',
    periodo_legislatura_rango_fecha: '',
  });

  const handleChange = (e) => {
    console.log('OnChange');
    const { name, value } = e.target;
    console.log({ name, value });
    setDictamenSeleccionado((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  //Trae los data desde la api
  const peticionGet = async () => {
    await axios
      .get(baseUrl)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Obtener tipo de deictamen de BD
  const tipoDictamenGet = async () => {
    await axios
      .get('http://45.56.114.13:8000/api/ctl/tipodictamen/')
      .then((response) => {
        setDictamenes(response.data);
      })
      .catch((error) => {
        console.log('ERROR: ', error);
      });
  };

  // Obtener comisiones de BD
  const comisionesGet = async () => {
    await axios
      .get('http://45.56.114.13:8000/api/ctl/comisiones/')
      .then((response) => {
        setComisiones(response.data);
      })
      .catch((error) => {
        console.log('ERROR: ', error);
      });
  };

  // Obtener periodos de BD
  const periodosGet = async () => {
    await axios
      .get('http://45.56.114.13:8000/api/ctl/periodolegislatura/')
      .then((response) => {
        setperiodosLegislatura(response.data);
      })
      .catch((error) => {
        console.log('ERROR: ', error);
      });
  };

  // Obtener Plenaria de BD
  const sesionesPlenariasGet = async () => {
    await axios
      .get('http://45.56.114.13:8000/api/ctl/sesionesplenarias/')
      .then((response) => {
        setSesionesPlenarias(response.data);
      })
      .catch((error) => {
        console.log('ERROR: ', error);
      });
  };

  const peticionPost = async (values) => {
    const { periodo_legislatura_rango_fecha, tipo_dictamen, nombre_comision, sesion_plenaria_num, ...otros } = values;
    let formData = {
      ...otros,
      periodo_legislatura: periodo_legislatura_rango_fecha,
      tipo: tipo_dictamen,
      id_comision: nombre_comision,
      id_sesion_plenaria: sesion_plenaria_num,
    };
    console.log('Post VALUES: ', formData);
    await axios
      .post(baseUrl, formData)
      .then((response) => {
        setData(data.concat(response.data));
        abrirCerrarModalInsertar();
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const peticionPut = async () => {
    console.log('Editar');
    await axios
      .put(baseUrl + 'modificar/' + dictamenSeleccionado.id + '/', dictamenSeleccionado)
      .then((response) => {
        peticionGet();
        abrirCerrarModalEditar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const peticionDelete = async () => {
    await axios
      .delete(baseUrl + 'modificar/' + dictamenSeleccionado.id + '/')
      .then((response) => {
        setData(data.filter((sesion) => sesion.id !== dictamenSeleccionado.id));
        abrirCerrarModalEliminar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const seleccionarSesion = (sesion, caso) => {
    console.log(sesion);
    console.log('Hola');
    setDictamenSeleccionado(sesion);
    caso === 'Editar' ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };

  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };

  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };

  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  useEffect(() => {
    peticionGet();
    tipoDictamenGet();
    comisionesGet();
    sesionesPlenariasGet();
    periodosGet();
  }, []);

  //Modal para insertar nuevo dictamen
  const [formularioEnviado, cambiarFormularioEnviado] = useState(false);

  const bodyInsertar = (
    <div className={styles.modal}>
      <h3>Agregar Dictamen</h3>
      <br />
      <Formik
        initialValues={{
          numero: '',
          tipo_dictamen: '',
          extracto: '',
          nombre_comision: '',
          documento_link: '',
          sesion_plenaria_num: '',
          periodo_legislatura_rango_fecha: '',
        }}

        validate={(values) => {
          const errors = {};
          console.log(errors);

          // Validacion numero (de uno a más)
          if (!values.numero) {
            errors.numero = 'Por favor ingresa el numero de la sesión plenaria';
          } else if (!/^[0-9]+$/.test(values.numero)) {
            errors.numero = 'El numero de la sesión plenaria solo puede contener números';
          }

          // Validacion de tipo_dictamen
          if (!values.tipo_dictamen) {
            errors.tipo_dictamen = 'Seleccionar el dictamen';
          }

          // Validacion de extracto
          if (!values.extracto) {
            errors.extracto = 'Por favor ingresa la sesión plenaria a la que pertenece';
          } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.extracto)) {
            errors.extracto = 'El nombre de la sesión plenaria solo puede contener letras y espacios';
          }

          // Validacion de periodo_legislatura_rango_fecha
          if (!values.periodo_legislatura_rango_fecha) {
            errors.periodo_legislatura_rango_fecha = 'Por favor ingresa la tipo_dictamen';
          } else if (!/^[0-9]{4}-[0-9]{4}$/.test(values.periodo_legislatura_rango_fecha)) {
            errors.periodo_legislatura_rango_fecha =
              'El periodo de legislatura debe de contener el siguiente formato:####-####';
          }

          return errors;
        }}

        onSubmit={(values, { resetForm }) => {
          peticionPost(values);
          resetForm();
          console.log('Formulario enviado', values);
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({ errors, touched, handleBlur, handleChange, handleSubmit, values }) => (
          
          <Form onSubmit='handleSubmit'>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor='numero'>Número</label>
                  <Field
                    id='numero'
                    name='numero'
                    type='text'
                    placeholder='Ingrese el número de dictamen'
                    onBlur={handleBlur}
                    onChange={handleChange}
                    className={styles.FieldMaterial}
                    validate={errors.numero && touched.numero}
                  />
                  {errors.numero && touched.numero && <div className={styles.ErrorMessage}> {errors.numero} </div>}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='tipo_dictamen'>Tipo de dictamen</label>
                  <Field
                    id='tipo_dictamen'
                    name='tipo_dictamen'
                    component='select'
                    multiple={false}
                    className={styles.FieldMaterial}
                    validate={errors.tipo_dictamen && touched.tipo_dictamen}
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione el tipo de dictamen ---
                    </option>
                    {dictamenes.map((dictamen) => (
                      <option key={dictamen.id} value={dictamen.id} className={styles.FieldMaterial}>
                        {dictamen.texto_tipo}
                      </option>
                    ))}
                  </Field>
                  {errors.tipo_dictamen && touched.tipo_dictamen && (
                    <div className={styles.ErrorMessage}> {errors.tipo_dictamen} </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='extracto'>Extracto</label>
                  <Field
                    id='extracto'
                    name='extracto'
                    type='text'
                    placeholder='Ingrese el extracto del dictamen'
                    onBlur={handleBlur}
                    onChange={handleChange}
                    className={styles.FieldMaterial}
                    validate={errors.extracto && touched.extracto}
                  />
                  {errors.extracto && touched.extracto && (
                    <div className={styles.ErrorMessage}> {errors.extracto} </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='nombre_comision'>Seleccione comisión</label>
                  <Field
                    id='nombre_comision'
                    name='nombre_comision'
                    component='select'
                    placeholder='--- Seleccione comisión ---'
                    multiple={false}
                    className={styles.FieldMaterial}
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione comisión ---
                    </option>
                    {comisiones.map((comision) => (
                      <option key={comision.id} value={comision.id} className={styles.FieldMaterial}>
                        {comision.nombre}
                      </option>
                    ))}
                  </Field>
                  {errors.nombre_comision && touched.nombre_comision && (
                    <div className={styles.ErrorMessage}> {errors.nombre_comision} </div>
                  )}
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor='documento_link'>Link del documento</label>
                  <Field
                    id='documento_link'
                    name='documento_link'
                    type='text'
                    placeholder='www.ejemplo.com'
                    onBlur={handleBlur}
                    onChange={handleChange}
                    className={styles.FieldMaterial}
                    validate={errors.documento_link && touched.documento_link}
                  />
                  {errors.documento_link && touched.documento_link && (
                    <div className={styles.ErrorMessage}> {errors.documento_link} </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='nombre_comision'>Seleccione número de sesión plenaria</label>
                  <Field
                    component='select'
                    id='sesion_plenaria_num'
                    name='sesion_plenaria_num'
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder='--- Seleccione comisión ---'
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione número de sesión plenaria ---
                    </option>
                    {sesionesPlenarias.map((sesionPlenaria) => (
                      <option key={sesionPlenaria.id} value={sesionPlenaria.id} className={styles.FieldMaterial}>
                        {sesionPlenaria.numero}
                      </option>
                    ))}
                  </Field>
                  {errors.sesion_plenaria_num && touched.sesion_plenaria_num && (
                    <div className={styles.ErrorMessage}> {errors.sesion_plenaria_num} </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='nombre_comision'>Seleccione Período de legislatura</label>
                  <Field
                    component='select'
                    id='periodo_legislatura_rango_fecha'
                    name='periodo_legislatura_rango_fecha'
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder='--- Seleccione Período de legislatura ---'
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione Período de legislaturaa ---
                    </option>
                    {periodosLegislatura.map((sesionPlenaria) => (
                      <option key={sesionPlenaria.id} value={sesionPlenaria.id} className={styles.FieldMaterial}>
                        {sesionPlenaria.rango_fecha}
                      </option>
                    ))}
                  </Field>
                </FormGroup>
                <br />
                <br />
                <Stack direction='row' spacing={2}>
                  <Button variant='contained' color='success' onClick={() => peticionPost(values)}>
                    Insertar
                  </Button>
                  <Button variant='contained' color='error' onClick={() => abrirCerrarModalInsertar()}>
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );

  const bodyEditar = (
    <div className={styles.modal}>
      <h3>Editar Sesión Plenaria</h3>
      <Formik
        validate={(values) => {
          const errors = {};

          //   // Validacion numero (de uno a más)
          //   if (!values.numero) {
          //     errors.numero = "Por favor ingresa el numero de la sesión plenaria";
          //   } else if (!/^[0-9]+$/.test(values.numero)) {
          //     errors.numero =
          //       "El numero de la sesión plenaria solo puede contener números";
          //   }

          //   // Validacion de tipo_dictamen
          //   if (!values.tipo_dictamen) {
          //     errors.tipo_dictamen = "Por favor ingresa la tipo_dictamen";
          //   }

          //   // Validacion de extracto
          //   if (!values.extracto) {
          //     errors.extracto =
          //       "Por favor ingresa la sesión plenaria a la que pertenece";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.extracto)) {
          //     errors.extracto =
          //       "El nombre de la sesión plenaria solo puede contener letras y espacios";
          //   }

          //   // Validacion de periodo_legislatura_rango_fecha
          //   if (!values.periodo_legislatura_rango_fecha) {
          //     errors.periodo_legislatura_rango_fecha =
          //       "Por favor ingresa la tipo_dictamen";
          //   } else if (
          //     !/^[0-9]{4}-[0-9]{4}$/.test(values.periodo_legislatura_rango_fecha)
          //   ) {
          //     errors.periodo_legislatura_rango_fecha =
          //       "El periodo de legislatura debe de contener el siguiente formato:####-####";
          //   }
          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          console.log('Submit');
          // peticionPut();
          // resetForm();
          // console.log('Formulario enviado');
          // cambiarFormularioEnviado(true);
          // setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({ errors, touched, handleBlur, handleSubmit, values }) => (
          <Form onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor='numero'>Número</label>
                  <Field
                    type='text'
                    id='numero'
                    name='numero'
                    placeholder='Ingrese el numero de dictamen'
                    className={styles.FieldMaterial}
                    validate={errors.numero && touched.numero}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={dictamenSeleccionado && dictamenSeleccionado.numero}
                  />
                  {errors.numero && touched.numero && <div className={styles.ErrorMessage}> {errors.numero} </div>}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='tipo_dictamen'>Tipo de dictamen</label>
                  <Field
                    component='select'
                    id='tipo_dictamen'
                    name='tipo'
                    multiple={false}
                    className={styles.FieldMaterial}
                    value={dictamenSeleccionado && dictamenSeleccionado.tipo}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    {dictamenes.map((dictamen) => (
                      <option key={dictamen.id} value={dictamen.id} className={styles.FieldMaterial}>
                        {dictamen.texto_tipo}
                      </option>
                    ))}
                  </Field>
                  {errors.tipo_dictamen && touched.tipo_dictamen && (
                    <div className={styles.ErrorMessage}> {errors.tipo_dictamen} </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='extracto'>Extracto</label>
                  <Field
                    type='text'
                    id='extracto'
                    name='extracto'
                    placeholder='Ingrese el extracto del dictamen'
                    className={styles.FieldMaterial}
                    validate={errors.extracto && touched.extracto}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={dictamenSeleccionado && dictamenSeleccionado.extracto}
                  />
                  {errors.extracto && touched.extracto && (
                    <div className={styles.ErrorMessage}> {errors.extracto} </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='nombre_comision'>Seleccione comisión</label>
                  <Field
                    component='select'
                    id='nombre_comision'
                    name='id_comision'
                    multiple={false}
                    className={styles.FieldMaterial}
                    value={dictamenSeleccionado && dictamenSeleccionado.id_comision}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    {comisiones.map((comision) => (
                      <option key={comision.id} value={comision.id} className={styles.FieldMaterial}>
                        {comision.nombre}
                      </option>
                    ))}
                  </Field>
                  {errors.nombre_comision && touched.nombre_comision && (
                    <div className={styles.ErrorMessage}> {errors.nombre_comision} </div>
                  )}
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor='documento_link'>Link del documento</label>
                  <Field
                    type='text'
                    id='documento_link'
                    name='documento_link'
                    placeholder='www.ejemplo.com'
                    className={styles.FieldMaterial}
                    validate={errors.documento_link && touched.documento_link}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={dictamenSeleccionado && dictamenSeleccionado.documento_link}
                  />
                  {errors.documento_link && touched.documento_link && (
                    <div className={styles.ErrorMessage}> {errors.documento_link} </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='nombre_comision'>Seleccione número de sesión plenaria</label>
                  <Field
                    component='select'
                    id='sesion_plenaria_num'
                    name='id_sesion_plenaria'
                    multiple={false}
                    className={styles.FieldMaterial}
                    value={dictamenSeleccionado && dictamenSeleccionado.id_sesion_plenaria}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    {sesionesPlenarias.map((sesionPlenaria) => (
                      <option key={sesionPlenaria.id} value={sesionPlenaria.id} className={styles.FieldMaterial}>
                        {sesionPlenaria.numero}
                      </option>
                    ))}
                  </Field>
                  {errors.sesion_plenaria_num && touched.sesion_plenaria_num && (
                    <div className={styles.ErrorMessage}> {errors.sesion_plenaria_num} </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='periodo_legislatura'>Período de legislatura</label>
                  <Field
                    component='select'
                    id='periodo_legislatura'
                    name='periodo_legislatura'
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder='--- Seleccione temporada gestión ---'
                    value={dictamenSeleccionado && dictamenSeleccionado.periodo_legislatura}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione periodo de legislatura ---
                    </option>
                    {periodosLegislatura.map((sesionPlenaria) => (
                      <option key={sesionPlenaria.id} value={sesionPlenaria.id} className={styles.FieldMaterial}>
                        {sesionPlenaria.rango_fecha}
                      </option>
                    ))}
                  </Field>
                </FormGroup>
                <br />
                <br />
                <Stack direction='row' spacing={2}>
                  <Button variant='contained' color='success' onClick={() => peticionPut()}>
                    Editar
                  </Button>
                  <Button variant='contained' color='error' onClick={() => abrirCerrarModalEditar()}>
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );

  //modal para eliminar una empresa
  const bodyEliminar = (
    <div className={styles.modal}>
      <p>
        Estás seguro que deseas eliminar el dictamen número <b>{dictamenSeleccionado && dictamenSeleccionado.numero}</b>
        ?{' '}
      </p>
      <div align='right'>
        <Button color='secondary' onClick={() => peticionDelete()}>
          Sí
        </Button>
        <Button onClick={() => abrirCerrarModalEliminar()}>No</Button>
      </div>
    </div>
  );

  return (
    <>
      <div className={table.table}>
        <MaterialTable
          columns={columns}
          data={data}
          title='Dictamenes'
          //Propiedades de los iconos eliminar y editar
          actions={[
            {
              icon: () => <AddIcon />,
              tooltip: 'Agregar empresa',
              isFreeAction: true,
              onClick: (event, rowData) => abrirCerrarModalInsertar(),
            },
            {
              icon: () => <EditIcon />,
              tooltip: 'Editar Empresa',
              onClick: (event, rowData) => seleccionarSesion(rowData, 'Editar'),
            },
            {
              icon: () => <DeleteIcon />,
              tooltip: 'Eliminar Empresa',
              onClick: (event, rowData) => seleccionarSesion(rowData, 'Eliminar'),
            },
            {
              icon: () => <PictureAsPdfIcon />, // you can pass icon too
              tooltip: 'Export to Pdf',
              onClick: () => downloadPdf(data),
              isFreeAction: true,
            },
          ]}
          //Propiedades de la tabla
          options={{
            filtering: true,
            grouping: true,
            sorting: true,
            thirdSortClick: false,
            search: true,
            searchFieldAlignment: 'right',
            searchAutoFocus: true,
            searchFieldVariant: 'standard',
            paging: true,
            pageSizeOptions: [5, 10, 20, 25, 50, 100],
            pageSize: 10,
            paginationType: 'stepped',
            showFirstLastPageButtons: false,
            paginationPosition: 'bottom',
            addRowPosition: 'first',
            actionsColumnIndex: -1,
            columnsButton: true,
          }}
          localization={{
            header: {
              actions: 'Acciones',
            },
          }}
        />
        <Modal open={modalInsertar} onClose={abrirCerrarModalInsertar}>
          {bodyInsertar}
        </Modal>

        <Modal open={modalEditar} onClose={abrirCerrarModalEditar}>
          {bodyEditar}
        </Modal>

        <Modal open={modalEliminar} onClose={abrirCerrarModalEliminar}>
          {bodyEliminar}
        </Modal>
      </div>
    </>
  );
};

export default Dictamenes;
