import React from "react";
import './Home.css';
//import {Comision,Dictamen,Diputado,SesionPlenaria,Votacion} from './control_legislativo';
// import Comision from "./control_legislativo/Comision";
import Comisiones from "./control_legislativo/Comisiones";
// import Dictamen from "./control_legislativo/Dictamen";
import Dictamenes from "./control_legislativo/Dictamenes";
import Diputado from "./control_legislativo/Diputado";
import Diputados from "./control_legislativo/Diputados";
// import SesionPlenaria from "./control_legislativo/SesionPlenaria";
import SesionesPlenarias from "./control_legislativo/SesionesPlenarias";
// import Votacion from "./control_legislativo/Votacion";
import Votaciones from "./control_legislativo/Votaciones";
//import {Empresa,Instituciones,Licitaciones,Procesos,Uaci} from './compras_publicas';
import Empresa from "./compras_publicas/Empresa";
import Instituciones from "./compras_publicas/Instituciones";
import Licitaciones from "./compras_publicas/Licitaciones";
import Procesos from "./compras_publicas/Procesos";
import Uaci from "./compras_publicas/Uaci";
import Login from "./Login";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Sidebar from "./Sidebar/Sidebar";
function Home() {
    return(
      <>
          <Router>
             <Sidebar/> 
              
                <Routes>
                  <Route exact path="/"/>
                  <Route exact path="/control_legislativo"/>
                  <Route exact path="/proyectos"/>
                  {/* <Route exact path="/sesionesplenarias" component={SesionPlenaria}/> */}
                  <Route exact path="/sesionesplenarias" component={SesionesPlenarias}/>
                  <Route exact path="/dictamenes" component={Dictamenes}/>
                  {/* <Route exact path="/diputados" component={Diputado}/> */}
                  <Route exact path="/diputados" component={Diputados}/>
                  <Route exact path="/votaciones"  component={Votaciones}/>
                  <Route exact path="/comisiones"  component={Comisiones}/> 
                  <Route exact path="/instituciones" component={Instituciones}/>
                  <Route exact path="/procesos"  component={Procesos}/>
                  <Route exact path="/uaci"  component={Uaci}/>
                  <Route exact path="/empresa" component={Empresa}/>
                  <Route exact path="/licitaciones"  component={Licitaciones}/>
                  <Route exact path="/login"  component={Login}/>
                </Routes>
             
            </Router>
      </>
    );
  };

export default Home;
