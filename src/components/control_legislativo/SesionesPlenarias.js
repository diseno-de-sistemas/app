import React, { useState, useEffect } from "react";
import axios from "axios";
import MaterialTable from "@material-table/core";
import { Formik, Form, Field } from "formik";
import { Modal, Grid, Input } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import "../Home.css";
import { FormGroup } from "@mui/material";
import jsPDF from "jspdf";
import "jspdf-autotable";
import AddIcon from "@mui/icons-material/Add";
import PictureAsPdfIcon from "@mui/icons-material/PictureAsPdf";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
//Columnas de la tabla
const columns = [
  // {
  //   title: "ID",
  //   field: "id",
  //   filterComponent: () => <Input placeholder="custom" />,
  //   grouping: true,
  // },
  {
    title: "Número",
    field: "numero",
    filterComponent: () => <Input placeholder="custom" />,
    grouping: true,
  },
  {
    title: "Fecha",
    field: "fecha",
    type: "date",
    filterComponent: () => <Input placeholder="custom" />,
    grouping: true,
    sorting: false,
  },
  {
    title: "Tipo",
    field: "tipo_sesionplenaria",
    filterComponent: () => <Input placeholder="custom" />,
    grouping: false,
    sorting: false,
  },
  {
    title: "Periodo de legislatura",
    field: "periodo_legislatura_rango_fecha",
    filtering: false,
    grouping: false,
    sorting: false,
  },
];
const downloadPdf = (dat) => {
  const doc = new jsPDF();
  doc.text("Sesiones Plenarias", 20, 10);
  doc.autoTable({
    theme: "grid",
    columns: columns.map((col) => ({ ...col, dataKey: col.field })),
    body: dat,
  });
  doc.save("SesionesPlenarias.pdf");
};
//url de la api
const baseUrl = "http://45.56.114.13:8000/api/ctl/sesionesplenarias/";

//Estilos del modal
const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 1000,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  FieldMaterial: {
    width: "100%",
  },
  ErrorMessage: {
    color: "#FF0000",
    fontStyle: "italic",
    fontFamily: "Times New Roman",
    fontSize: "15px",
  },
}));

//Estilos para la DataTable
const tableStyles = makeStyles((theme) => ({
  table: {
    marginTop: theme.spacing(3),
    "& thead th": {
      fontWeight: "600",
      color: theme.palette.primary.main,
      backgroundColor: " #d5d6dc ",
    },
    "& tbody td": {
      fontWeight: "300",
    },
    "& tbody tr:hover": {
      backgroundColor: "#fffbf2",
      cursor: "pointer",
    },
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderBlockStyle: "solid",
    borderBlockWidth: "medium",
  },
}));

const SesionesPlenarias = () => {
  const styles = useStyles();
  const table = tableStyles();
  const [data, setData] = useState([]);
  const [sesionesPlenariass, setsesionesPlenarias] = useState([]);
  const [periodosLegislatura, setperiodosLegislatura] = useState([]);
  const [partidos, setPartidos] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [sesionSeleccionado, setSesionSeleccionado] = useState({
    id: "",
    numero: "",
    fecha: "",
    tipo_sesionplenaria: "",
    periodo_legislatura_rango_fecha: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSesionSeleccionado((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  //Trae los data desde la api
  const peticionGet = async () => {
    await axios
      .get(baseUrl)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Obtener comisiones de BD
  const tipoSesionPlenariaGet = async () => {
    await axios
      .get("http://45.56.114.13:8000/api/ctl/tiposesionplenaria/")
      .then((response) => {
        setsesionesPlenarias(response.data);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };

  // Obtener partidos de BD
  const partidosGet = async () => {
    await axios
      .get("http://45.56.114.13:8000/api/ctl/partidopolitico/")
      .then((response) => {
        setPartidos(response.data);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };

  // Obtener periodos de BD
  const periodosGet = async () => {
    await axios
      .get("http://45.56.114.13:8000/api/ctl/periodolegislatura/")
      .then((response) => {
        setperiodosLegislatura(response.data);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };

  const peticionPost = async (values) => {
    console.log("VALUES: ", values);
    await axios
      .post(baseUrl, {
        numero: values.numero,
        fecha: values.fecha,
        tipo: values.tipo_sesionplenaria,
        periodo_legislatura: values.periodo_legislatura_rango_fecha,
      })
      .then((response) => {
        setData(data.concat(response.data));
        abrirCerrarModalInsertar();
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // // Testeo de valores enviados desde el submit
  // const peticionPost = (values) => {
  //   console.log("VALUES: ", values);
  //   console.log("X-CSRFToken: ", csrftoken);
  // };

  const peticionPut = async (values) => {
    const headers = { "Content-Type": "application/json" };
    // console.log("SELECCIONADO: ", sesionSeleccionado);
    // console.log("VALUES: ", values);
    await axios
      .put(
        baseUrl + "modificar/" + sesionSeleccionado.id + "/",
        {
          numero: sesionSeleccionado.numero,
          fecha: sesionSeleccionado.fecha,
          tipo: sesionSeleccionado.tipo,
          periodo_legislatura: sesionSeleccionado.periodo_legislatura,
        },
        headers
      )
      .then((response) => {
        var dataNueva = data;
        dataNueva.map((sesion) => {
          if (sesion.id === sesionSeleccionado.id) {
            sesion.numero = sesionSeleccionado.numero;
            sesion.fecha = sesionSeleccionado.fecha;
            sesion.tipo_sesionplenaria = sesionSeleccionado.tipo_sesionplenaria;
            sesion.periodo_legislatura_rango_fecha =
              sesionSeleccionado.periodo_legislatura_rango_fecha;
          }
        });
        setData(dataNueva);
        abrirCerrarModalEditar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const peticionDelete = async () => {
    await axios
      .delete(baseUrl + "modificar/" + sesionSeleccionado.id + "/")
      .then((response) => {
        setData(data.filter((sesion) => sesion.id !== sesionSeleccionado.id));
        abrirCerrarModalEliminar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const seleccionarSesion = (sesion, caso) => {
    setSesionSeleccionado(sesion);
    caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };

  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };

  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };

  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  useEffect(() => {
    peticionGet();
    tipoSesionPlenariaGet();
    partidosGet();
    periodosGet();
  }, []);

  useEffect(() => {
    if (modalInsertar === false || modalEditar === false) peticionGet();
  }, [modalInsertar, modalEditar]);

  //Modal para insertar una sesion plenaria
  const [formularioEnviado, cambiarFormularioEnviado] = useState(false);
  const bodyInsertar = (
    <div className={styles.modal}>
      <h3>Agregar Sesion Plenaria</h3>
      <br />
      <Formik
        initialValues={{
          numero: "",
          fecha: "",
          tipo_sesionplenaria: "",
          periodo_legislatura_rango_fecha: "",
        }}
        validate={(values) => {
          const errors = {};

          // Validacion numero (de uno a más)
          if (!values.numero) {
            errors.numero = "Por favor ingresa el numero de la sesión plenaria";
          } else if (!/^[0-9]+$/.test(values.numero)) {
            errors.numero =
              "El numero de la sesión plenaria solo puede contener números";
          }

          // Validacion de fecha
          if (!values.fecha) {
            errors.fecha = "Por favor ingresa la fecha";
          }

          // // Validacion de tipo_sesionplenaria
          // if (!values.tipo_sesionplenaria) {
          //   errors.tipo_sesionplenaria =
          //     "Por favor ingresa el tipo de sesión plenaria a la que pertenece";
          // } else if (
          //   !/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.tipo_sesionplenaria)
          // ) {
          //   errors.tipo_sesionplenaria =
          //     "El nombre de la sesión plenaria solo puede contener letras y espacios";
          // }

          // Validacion de periodo_legislatura_rango_fecha
          if (!values.periodo_legislatura_rango_fecha) {
            errors.periodo_legislatura_rango_fecha =
              "Por favor el periodo de legislatura";
          } else if (
            !/^[0-9]{4}-[0-9]{4}$/.test(values.periodo_legislatura_rango_fecha)
          ) {
            errors.periodo_legislatura_rango_fecha =
              "El periodo de legislatura debe de contener el siguiente formato:####-####";
          }
          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          peticionPost(values);
          resetForm();
          console.log("Formulario enviado", values);
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          values,
        }) => (
          <Form>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor="numero">Número</label>
                  <Field
                    type="text"
                    id="numero"
                    name="numero"
                    placeholder="Ingrese el numero de la sesión plenaria"
                    className={styles.FieldMaterial}
                    validate={errors.numero && touched.numero}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.numero && touched.numero && (
                    <div className={styles.ErrorMessage}> {errors.numero} </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor="fecha">Fecha</label>
                  <Field
                    type="date"
                    id="fecha"
                    name="fecha"
                    placeholder="7/11/2021"
                    className={styles.FieldMaterial}
                    validate={errors.fecha && touched.fecha}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.fecha && touched.fecha && (
                    <div className={styles.ErrorMessage}> {errors.fecha} </div>
                  )}
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor="tipo_sesionplenaria">
                    Tipo de sesión plenaria
                  </label>
                  <Field
                    component="select"
                    id="tipo_sesionplenaria"
                    name="tipo_sesionplenaria"
                    multiple={false}
                    className={styles.FieldMaterial}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione el tipo de sesión plenaria ---
                    </option>
                    {sesionesPlenariass.map((sesionPlenaria) => (
                      <option
                        key={sesionPlenaria.id}
                        value={sesionPlenaria.id}
                        className={styles.FieldMaterial}
                      >
                        {sesionPlenaria.texto_tipo}
                      </option>
                    ))}
                  </Field>
                  {errors.tipo_sesionplenaria &&
                    touched.tipo_sesionplenaria && (
                      <div className={styles.ErrorMessage}>
                        {" "}
                        {errors.tipo_sesionplenaria}{" "}
                      </div>
                    )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor="nombre_comision">
                    Seleccione Período de legislatura
                  </label>
                  <Field
                    component="select"
                    id="periodo_legislatura_rango_fecha"
                    name="periodo_legislatura_rango_fecha"
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder="--- Seleccione Período de legislatura ---"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione Período de legislatura ---
                    </option>
                    {periodosLegislatura.map((sesionPlenaria) => (
                      <option
                        key={sesionPlenaria.id}
                        value={sesionPlenaria.id}
                        className={styles.FieldMaterial}
                      >
                        {sesionPlenaria.rango_fecha}
                      </option>
                    ))}
                  </Field>
                </FormGroup>
                <br />
                <br />
                <Stack direction="row" spacing={2}>
                  <Button
                    variant="contained"
                    color="success"
                    onClick={() => peticionPost(values)}
                  >
                    Insertar
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={() => abrirCerrarModalInsertar()}
                  >
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );

  //Modal para editar una sesión plenaria
  const bodyEditar = (
    <div className={styles.modal}>
      <h3>Editar Sesión Plenaria</h3>
      <Formik
        validate={(values) => {
          const errors = {};

          // // Validacion numero (de uno a más)
          // if (!values.numero) {
          //   errors.numero = "Por favor ingresa el numero de la sesión plenaria";
          // } else if (!/^[0-9]+$/.test(values.numero)) {
          //   errors.numero =
          //     "El numero de la sesión plenaria solo puede contener números";
          // }

          // //Validacion de fecha
          // if (!values.fecha) {
          //   errors.fecha = "Por favor ingresa la fecha";
          // }

          // // Validacion de tipo_sesionplenaria
          // if (!values.tipo_sesionplenaria) {
          //   errors.tipo_sesionplenaria =
          //     "Por favor ingresa el tipo de sesión plenaria a la que pertenece";
          // } else if (
          //   !/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.tipo_sesionplenaria)
          // ) {
          //   errors.tipo_sesionplenaria =
          //     "El nombre de la sesión plenaria solo puede contener letras y espacios";
          // }

          // // Validacion de periodo_legislatura_rango_fecha
          // if (!values.periodo_legislatura_rango_fecha) {
          //   errors.periodo_legislatura_rango_fecha =
          //     "Por favor el periodo de legislatura";
          // } else if (
          //   !/^[0-9]{4}-[0-9]{4}$/.test(values.periodo_legislatura_rango_fecha)
          // ) {
          //   errors.periodo_legislatura_rango_fecha =
          //     "El periodo de legislatura debe de contener el siguiente formato:####-####";
          // }
          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          peticionPut();
          resetForm();
          console.log("Formulario enviado");
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({ errors, touched, handleBlur, handleSubmit, values }) => (
          <Form onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor="numero">Número</label>
                  <Field
                    type="text"
                    id="numero"
                    name="numero"
                    placeholder="Ingrese el numero de la sesión plenaria"
                    className={styles.FieldMaterial}
                    validate={errors.numero && touched.numero}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={sesionSeleccionado && sesionSeleccionado.numero}
                  />
                  {errors.numero && touched.numero && (
                    <div className={styles.ErrorMessage}> {errors.numero} </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor="fecha">Fecha</label>
                  <Field
                    type="date"
                    id="fecha"
                    name="fecha"
                    placeholder="7/11/2021"
                    className={styles.FieldMaterial}
                    validate={errors.fecha && touched.fecha}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={sesionSeleccionado && sesionSeleccionado.fecha}
                  />
                  {errors.fecha && touched.fecha && (
                    <div className={styles.ErrorMessage}> {errors.fecha} </div>
                  )}
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor="tipo">
                    Tipo de sesión plenaria
                  </label>
                  <Field
                    component="select"
                    id="tipo"
                    name="tipo"
                    multiple={false}
                    className={styles.FieldMaterial}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder="--- Seleccione tipo de sesion plenaria ---"
                    value={
                      sesionSeleccionado &&
                      sesionSeleccionado.tipo
                    }
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione tipo de sesion plenaria ---
                    </option>
                    {sesionesPlenariass.map((sesionPlenaria) => (
                      <option
                        key={sesionPlenaria.id}
                        value={sesionPlenaria.id}
                        className={styles.FieldMaterial}
                      >
                        {sesionPlenaria.texto_tipo}
                      </option>
                    ))}
                  </Field>
                  {errors.tipo &&
                    touched.tipo && (
                      <div className={styles.ErrorMessage}>
                        {" "}
                        {errors.tipo}{" "}
                      </div>
                    )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor="periodo_legislatura">
                    Período de legislatura
                  </label>
                  <Field
                    component="select"
                    id="periodo_legislatura"
                    name="periodo_legislatura"
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder="--- Seleccione temporada gestión ---"
                    value={
                      sesionSeleccionado &&
                      sesionSeleccionado.periodo_legislatura
                    }
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione periodo de legislatura ---
                    </option>
                    {periodosLegislatura.map((sesionPlenaria) => (
                      <option
                        key={sesionPlenaria.id}
                        value={sesionPlenaria.id}
                        className={styles.FieldMaterial}
                      >
                        {sesionPlenaria.rango_fecha}
                      </option>
                    ))}
                  </Field>
                </FormGroup>
                <br />
                <br />
                <Stack direction="row" spacing={2}>
                  <Button
                    variant="contained"
                    color="success"
                    onClick={() => peticionPut(values)}
                  >
                    Editar
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={() => abrirCerrarModalEditar()}
                  >
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );

  //modal para eliminar una empresa
  const bodyEliminar = (
    <div className={styles.modal}>
      <p>
        Estás seguro que deseas eliminar la sesión plenaria número{" "}
        <b>{sesionSeleccionado && sesionSeleccionado.numero}</b>?{" "}
      </p>
      <div align="right">
        <Button color="secondary" onClick={() => peticionDelete()}>
          Sí
        </Button>
        <Button onClick={() => abrirCerrarModalEliminar()}>No</Button>
      </div>
    </div>
  );

  return (
    <>
      <div className={table.table}>
        <MaterialTable
          columns={columns}
          data={data}
          title="Sesiones plenarias"
          //Propiedades de los iconos eliminar y editar
          actions={[
            {
              icon: () => <AddIcon />,
              tooltip: "Agregar empresa",
              isFreeAction: true,
              onClick: (event, rowData) => abrirCerrarModalInsertar(),
            },
            {
              icon: () => <EditIcon />,
              tooltip: "Editar Empresa",
              onClick: (event, rowData) => seleccionarSesion(rowData, "Editar"),
            },
            {
              icon: () => <DeleteIcon />,
              tooltip: "Eliminar Empresa",
              onClick: (event, rowData) =>
                seleccionarSesion(rowData, "Eliminar"),
            },
            {
              icon: () => <PictureAsPdfIcon />, // you can pass icon too
              tooltip: "Export to Pdf",
              onClick: () => downloadPdf(data),
              isFreeAction: true,
            },
          ]}
          //Propiedades de la tabla
          options={{
            filtering: true,
            grouping: true,
            sorting: true,
            thirdSortClick: false,
            search: true,
            searchFieldAlignment: "right",
            searchAutoFocus: true,
            searchFieldVariant: "standard",
            paging: true,
            pageSizeOptions: [5, 10, 20, 25, 50, 100],
            pageSize: 5,
            paginationType: "stepped",
            showFirstLastPageButtons: false,
            paginationPosition: "bottom",
            addRowPosition: "first",
            actionsColumnIndex: -1,
            columnsButton: true,
          }}
          localization={{
            header: {
              actions: "Acciones",
            },
          }}
        />
        <Modal open={modalInsertar} onClose={abrirCerrarModalInsertar}>
          {bodyInsertar}
        </Modal>

        <Modal open={modalEditar} onClose={abrirCerrarModalEditar}>
          {bodyEditar}
        </Modal>

        <Modal open={modalEliminar} onClose={abrirCerrarModalEliminar}>
          {bodyEliminar}
        </Modal>
      </div>
    </>
  );
};

export default SesionesPlenarias;
