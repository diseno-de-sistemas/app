import React, { useState, useEffect } from "react";
import axios from "axios";
import MaterialTable from "@material-table/core";
import { Formik, Form, Field } from "formik";
import { Modal, Grid, Input } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import "../Home.css";
import { FormGroup } from "@mui/material";
import jsPDF from "jspdf";
import "jspdf-autotable";
import AddIcon from "@mui/icons-material/Add";
import PictureAsPdfIcon from "@mui/icons-material/PictureAsPdf";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
const columns = [
  // {
  //   title: "ID",
  //   field: "id",
  //   filterComponent: () => <Input placeholder="custom" />,
  //   grouping: true,
  // },
  {
    title: "Número de dictamen",
    field: "numero_dictamen",
    filterComponent: () => <Input placeholder="custom" />,
    grouping: true,
  },
  {
    title: "Diputado",
    field: "nombre_diputado",
    filterComponent: () => <Input placeholder="custom" />,
    grouping: true,
    sorting: false,
  },
  {
    title: "Respuesta",
    field: "respuesta_diputado",
    filterComponent: () => <Input placeholder="custom" />,
    grouping: false,
    sorting: false,
  },
];
const downloadPdf = (dat) => {
  const doc = new jsPDF();
  doc.text("Votaciones", 20, 10);
  doc.autoTable({
    theme: "grid",
    columns: columns.map((col) => ({ ...col, dataKey: col.field })),
    body: dat,
  });
  doc.save("Votaciones.pdf");
};
//url de la api
const baseUrl = "http://45.56.114.13:8000/api/ctl/votaciones/";

//Estilos del modal
const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 1000,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  FieldMaterial: {
    width: "100%",
  },
  ErrorMessage: {
    color: "#FF0000",
    fontStyle: "italic",
    fontFamily: "Times New Roman",
    fontSize: "15px",
  },
}));

//Estilos para la DataTable
const tableStyles = makeStyles((theme) => ({
  table: {
    marginTop: theme.spacing(3),
    "& thead th": {
      fontWeight: "600",
      color: theme.palette.primary.main,
      backgroundColor: " #d5d6dc ",
    },
    "& tbody td": {
      fontWeight: "300",
    },
    "& tbody tr:hover": {
      backgroundColor: "#fffbf2",
      cursor: "pointer",
    },
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderBlockStyle: "solid",
    borderBlockWidth: "medium",
  },
}));

function Votaciones() {
  const styles = useStyles();
  const table = tableStyles();
  const [data, setData] = useState([]);
  const [dictamenes, setDictamenes] = useState([]);
  const [diputados, setDiputados] = useState([]);
  const [respuestas, setRespuestas] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [votacionSeleccionado, setvotacionSeleccionado] = useState({
    id: "",
    numero_dictamen: "",
    nombre_diputado: "",
    respuesta_diputado: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setvotacionSeleccionado((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  //Trae los data desde la api
  const peticionGet = async () => {
    await axios
      .get(baseUrl)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Obtener dictamenes de BD
  const dictamenesGet = async () => {
    await axios
      .get("http://45.56.114.13:8000/api/ctl/dictamenes/")
      .then((response) => {
        setDictamenes(response.data);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };

  // Obtener diputados de BD
  const diputadosGet = async () => {
    await axios
      .get("http://45.56.114.13:8000/api/ctl/diputados/")
      .then((response) => {
        setDiputados(response.data);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };

  // Obtener respuestas de BD
  const respuestasGet = async () => {
    await axios
      .get("http://45.56.114.13:8000/api/ctl/respuestavotacion/")
      .then((response) => {
        setRespuestas(response.data);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };

  const peticionPost = async (values) => {
    console.log("VALUES: ", values);
    await axios
      .post(baseUrl, values)
      .then((response) => {
        setData(data.concat(response.data));
        abrirCerrarModalInsertar();
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // // Testeo de valores enviados desde el submit
  // const peticionPost = (values) => {
  //   console.log("VALUES: ", values);
  //   //console.log("X-CSRFToken: ", csrftoken);
  // };

  const peticionPut = async (values) => {
    await axios
      .put(baseUrl + "modificar/" + values.id + "/", votacionSeleccionado)
      .then((response) => {
        peticionGet();
        abrirCerrarModalEditar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const peticionDelete = async (values) => {
    await axios
      .delete(baseUrl + "modificar/" + votacionSeleccionado.id + "/")
      .then((response) => {
        setData(
          data.filter((votacion) => votacion.id !== votacionSeleccionado.id)
        );
        abrirCerrarModalEliminar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const seleccionarDiputado = (votacion, caso) => {
    setvotacionSeleccionado(votacion);
    caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };

  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };

  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };

  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  useEffect(() => {
    peticionGet();
    dictamenesGet();
    diputadosGet();
    respuestasGet();
  }, []);

  const mapvalues = (values) => {
    console.log(values);
    var data = {
      id_dictamen: values.numero_dictamen,
      id_diputado: values.nombre_diputado,
      respuesta: values.respuesta_diputado,
    };
    console.log(data, "data");
    peticionPost(data);
  };

  const setMapValuePut = (values) => {
    console.log(votacionSeleccionado);
    const data = {
      id: votacionSeleccionado.id,
      id_dictamen: votacionSeleccionado.id_dictamen,
      id_diputado: votacionSeleccionado.id_diputado,
      respuesta: votacionSeleccionado.respuesta_diputado,
    };
    peticionPut(data);
  };

  //Modal para insertar un diputado
  const [formularioEnviado, cambiarFormularioEnviado] = useState(false);
  const bodyInsertar = (
    <div className={styles.modal}>
      <h3>Agregar Voto</h3>
      <br />
      <Formik
        initialValues={{
          numero_dictamen: "",
          nombre_diputado: "",
          respuesta_diputado: "",
        }}
        validate={(values) => {
          const errors = {};

          //   // Validacion nombre
          //   if (!values.nombre) {
          //     errors.nombre = "Por favor ingresa el nombre del diputado";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre)) {
          //     errors.nombre =
          //       "El nombre del diputado solo puede contener letras y espacios";
          //   }

          //   // Validacion nombre_comision
          //   if (!values.nombre_comision) {
          //     errors.nombre_comision =
          //       "Por favor ingresa la comisión a la que pertenece";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_comision)) {
          //     errors.nombre_comision =
          //       "El nombre de la comisión solo puede contener letras y espacios";
          //   }

          //   // Validacion de nombre_partido
          //   if (!values.nombre_partido) {
          //     errors.nombre_partido =
          //       "Por favor ingresa el partido político al que pertenece";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_partido)) {
          //     errors.nombre_partido =
          //       "El nombre del partido político solo puede contener letras y espacios";
          //   }

          //   // Validacion de periodo_legislatura_rango_fecha
          //   if (!values.periodo_legislatura_rango_fecha) {
          //     errors.periodo_legislatura_rango_fecha =
          //       "Por favor ingresa la fecha";
          //   } else if (
          //     !/^[0-9]{4}-[0-9]{4}$/.test(values.periodo_legislatura_rango_fecha)
          //   ) {
          //     errors.periodo_legislatura_rango_fecha =
          //       "El periodo de legislatura debe de contener el siguiente formato:####-####";
          //   }
          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          console.log(values);
          peticionPost(values);
          resetForm();
          console.log("Formulario enviado", values);
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          values,
        }) => (
          <Form>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor="numero_dictamen">
                    Seleccione número de dictamen
                  </label>
                  <Field
                    component="select"
                    id="numero_dictamen"
                    name="numero_dictamen"
                    multiple={false}
                    className={styles.FieldMaterial}
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione número de dictamen ---
                    </option>
                    {dictamenes.map((dictamen) => (
                      <option
                        key={dictamen.id}
                        value={dictamen.id}
                        className={styles.FieldMaterial}
                      >
                        {dictamen.numero}
                      </option>
                    ))}
                  </Field>
                  {errors.numero_dictamen && touched.numero_dictamen && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.numero_dictamen}{" "}
                    </div>
                  )}
                </FormGroup>
                <FormGroup>
                  <label htmlFor="nombre_diputado">
                    Seleccione el diputado
                  </label>
                  <Field
                    component="select"
                    id="nombre_diputado"
                    name="nombre_diputado"
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder="--- Seleccione comisión ---"
                  >
                    <option value="" className="text-secondary">
                      --- Seleccione el diputado ---
                    </option>
                    {diputados.map((diputado) => (
                      <option
                        key={diputado.id}
                        value={diputado.id}
                        className={styles.FieldMaterial}
                      >
                        {diputado.nombre}
                      </option>
                    ))}
                  </Field>
                  {errors.nombre_diputado && touched.nombre_diputado && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.nombre_diputado}{" "}
                    </div>
                  )}
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor="respuesta_diputado">
                    Respuesta de diputado
                  </label>
                  <Field
                    component="select"
                    id="respuesta_diputado"
                    name="respuesta_diputado"
                    multiple={false}
                    className={styles.FieldMaterial}
                  >
                    <option value="" className="text-secondary">
                      --- Respuesta de diputado ---
                    </option>
                    {respuestas.map((respuesta) => (
                      <option
                        key={respuesta.id}
                        value={respuesta.id}
                        className={styles.FieldMaterial}
                      >
                        {respuesta.texto_respuesta}
                      </option>
                    ))}
                  </Field>
                  {errors.respuesta_diputado && touched.respuesta_diputado && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.respuesta_diputado}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <br />
                <Stack direction="row" spacing={2}>
                  <Button
                    variant="contained"
                    color="success"
                    onClick={() => mapvalues(values)}

                    // onClick={() => peticionPost(values)}
                  >
                    Insertar
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={() => abrirCerrarModalInsertar()}
                  >
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );

  //Modal para editar votacion
  const bodyEditar = (
    <div className={styles.modal}>
      <h3>Editar Voto</h3>
      <Formik
        validate={(values) => {
          const errors = {};

          //   // Validacion nombre
          //   if (!values.nombre) {
          //     errors.nombre = "Por favor ingresa el nombre del diputado";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre)) {
          //     errors.nombre =
          //       "El nombre del diputado solo puede contener letras y espacios";
          //   }

          //   // Validacion nombre_comision
          //   if (!values.nombre_comision) {
          //     errors.nombre_comision =
          //       "Por favor ingresa la comisión a la que pertenece";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_comision)) {
          //     errors.nombre_comision =
          //       "El nombre de la comisión solo puede contener letras y espacios";
          //   }

          //   // Validacion de nombre_partido
          //   if (!values.nombre_partido) {
          //     errors.nombre_partido =
          //       "Por favor ingresa el partido político al que pertenece";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_partido)) {
          //     errors.nombre_partido =
          //       "El nombre del partido político solo puede contener letras y espacios";
          //   }

          //   // Validacion de periodo_legislatura_rango_fecha
          //   if (!values.periodo_legislatura_rango_fecha) {
          //     errors.periodo_legislatura_rango_fecha =
          //       "Por favor ingresa la fecha";
          //   } else if (
          //     !/^[0-9]{4}-[0-9]{4}$/.test(values.periodo_legislatura_rango_fecha)
          //   ) {
          //     errors.periodo_legislatura_rango_fecha =
          //       "El periodo de legislatura debe de contener el siguiente formato:####-####";
          //   }
          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          peticionPut();
          resetForm();
          console.log("Formulario enviado");
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({ errors, touched, handleBlur, handleSubmit, values }) => (
          <Form onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor="numero_dictamen">
                    Seleccione número de dictamen
                  </label>
                  <Field
                    component="select"
                    id="numero_dictamen"
                    name="id_dictamen"
                    multiple={false}
                    className={styles.FieldMaterial}
                    validate={errors.numero && touched.numero}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={
                      votacionSeleccionado && votacionSeleccionado.id_dictamen
                    }
                  >
                    {/* <option value="" className="text-secondary">
                      --- Seleccione número de dictamen ---
                    </option> */}
                    {dictamenes.map((dictamen) => (
                      <option
                        key={dictamen.id}
                        value={dictamen.id}
                        className={styles.FieldMaterial}
                      >
                        {dictamen.numero}
                      </option>
                    ))}
                  </Field>
                  {errors.numero_dictamen && touched.numero_dictamen && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.numero_dictamen}{" "}
                    </div>
                  )}
                </FormGroup>

                <FormGroup>
                  <label htmlFor="nombre_diputado">
                    Seleccione el diputado
                  </label>
                  <Field
                    component="select"
                    id="id_diputado"
                    name="id_diputado"
                    multiple={false}
                    className={styles.FieldMaterial}
                    validate={errors.numero && touched.numero}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={
                      votacionSeleccionado && votacionSeleccionado.id_diputado
                    }
                  >
                    {/* <option value="" className="text-secondary">
                      --- Seleccione el diputado ---
                    </option> */}
                    {diputados.map((diputado) => (
                      <option
                        key={diputado.id}
                        value={diputado.id}
                        className={styles.FieldMaterial}
                      >
                        {diputado.nombre}
                      </option>
                    ))}
                  </Field>
                  {errors.nombre_diputado && touched.nombre_diputado && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.nombre_diputado}{" "}
                    </div>
                  )}
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor="respuesta_diputado">
                    Respuesta de diputado
                  </label>
                  <Field
                    component="select"
                    id="respuesta_diputado"
                    name="respuesta"
                    multiple={false}
                    className={styles.FieldMaterial}
                    validate={errors.numero && touched.numero}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={
                      votacionSeleccionado && votacionSeleccionado.respuesta
                    }
                  >
                    {/* <option value="" className="text-secondary">
                      --- Respuesta de diputado ---
                    </option> */}
                    {respuestas.map((respuesta) => (
                      <option
                        key={respuesta.id}
                        value={respuesta.id}
                        className={styles.FieldMaterial}
                      >
                        {respuesta.texto_respuesta}
                      </option>
                    ))}
                  </Field>
                  {errors.respuesta_diputado && touched.respuesta_diputado && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.respuesta_diputado}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <br />
                <Stack direction="row" spacing={2}>
                  <Button
                    variant="contained"
                    color="success"
                    // onClick={() => peticionPut(values)}
                    onClick={() => setMapValuePut()}
                  >
                    Editar
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={() => abrirCerrarModalEditar()}
                  >
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );

  //modal para eliminar una empresa
  const bodyEliminar = (
    <div className={styles.modal}>
      <p>
        Estás seguro que deseas eliminar el resgistro de la votación para el
        dictamen con número{" "}
        <b>{votacionSeleccionado && votacionSeleccionado.numero_dictamen}</b>?{" "}
      </p>
      <div align="right">
        <Button color="secondary" onClick={() => peticionDelete()}>
          Sí
        </Button>
        <Button onClick={() => abrirCerrarModalEliminar()}>No</Button>
      </div>
    </div>
  );

  return (
    <>
      <div className={table.table}>
        <MaterialTable
          columns={columns}
          data={data}
          title="Votaciones"
          //Propiedades de los iconos eliminar y editar
          actions={[
            {
              icon: () => <AddIcon />,
              tooltip: "Agregar empresa",
              isFreeAction: true,
              onClick: (event, rowData) => abrirCerrarModalInsertar(),
            },
            {
              icon: () => <EditIcon />,
              tooltip: "Editar Empresa",
              onClick: (event, rowData) =>
                seleccionarDiputado(rowData, "Editar"),
            },
            {
              icon: () => <DeleteIcon />,
              tooltip: "Eliminar Empresa",
              onClick: (event, rowData) =>
                seleccionarDiputado(rowData, "Eliminar"),
            },
            {
              icon: () => <PictureAsPdfIcon />, // you can pass icon too
              tooltip: "Export to Pdf",
              onClick: () => downloadPdf(data),
              isFreeAction: true,
            },
          ]}
          //Propiedades de la tabla
          options={{
            filtering: true,
            grouping: true,
            sorting: true,
            thirdSortClick: false,
            search: true,
            searchFieldAlignment: "right",
            searchAutoFocus: true,
            searchFieldVariant: "standard",
            paging: true,
            pageSizeOptions: [5, 10, 20, 25, 50, 100],
            pageSize: 5,
            paginationType: "stepped",
            showFirstLastPageButtons: false,
            paginationPosition: "bottom",
            addRowPosition: "first",
            actionsColumnIndex: -1,
            columnsButton: true,
          }}
          localization={{
            header: {
              actions: "Acciones",
            },
          }}
        />
        <Modal open={modalInsertar} onClose={abrirCerrarModalInsertar}>
          {bodyInsertar}
        </Modal>

        <Modal open={modalEditar} onClose={abrirCerrarModalEditar}>
          {bodyEditar}
        </Modal>

        <Modal open={modalEliminar} onClose={abrirCerrarModalEliminar}>
          {bodyEliminar}
        </Modal>
      </div>
    </>
  );
}

export default Votaciones;