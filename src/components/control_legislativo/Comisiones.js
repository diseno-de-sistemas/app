import React, { useState, useEffect } from "react";
import axios from "axios";
import MaterialTable from "@material-table/core";
import { Formik, Form, Field } from "formik";
import { Modal, Grid, Input } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import "../Home.css";
import { FormGroup } from "@mui/material";
import jsPDF from "jspdf";
import "jspdf-autotable";
import AddIcon from '@mui/icons-material/Add';
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
const columns = [
  // {
  //   title: "ID",
  //   field: "id",
  //   filterComponent: () => <Input placeholder="custom" />,
  //   grouping: true,
  // },
  {
    title: "nombre",
    field: "nombre",
    filterComponent: () => <Input placeholder="custom" />,
    grouping: true,
  },
  {
    title: "Tipo",
    field: "tipo_comision",
    filterComponent: () => <Input placeholder="custom" />,
    grouping: true,
    sorting: false,
  },
];
const downloadPdf = (dat) => {
  const doc = new jsPDF()
  doc.text("Comisiones", 20, 10)
  doc.autoTable({
    theme: "grid",
    columns: columns.map(col => ({ ...col, dataKey: col.field })),
    body: dat
  })
  doc.save('Comisiones.pdf')
}
//url de la api
const baseUrl = "http://45.56.114.13:8000/api/ctl/comisiones/";

//Estilos del modal
const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 1000,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  FieldMaterial: {
    width: "100%",
  },
  ErrorMessage: {
    color: "#FF0000",
    fontStyle: "italic",
    fontFamily: "Times New Roman",
    fontSize: "15px",
  },
}));

//Estilos para la DataTable
const tableStyles = makeStyles((theme) => ({
  table: {
    marginTop: theme.spacing(3),
    "& thead th": {
      fontWeight: "600",
      color: theme.palette.primary.main,
      backgroundColor: " #d5d6dc ",
    },
    "& tbody td": {
      fontWeight: "300",
    },
    "& tbody tr:hover": {
      backgroundColor: "#fffbf2",
      cursor: "pointer",
    },
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderBlockStyle: "solid",
    borderBlockWidth: "medium",
  },
}));

function Comisiones() {
  const styles = useStyles();
  const table = tableStyles();
  const [data, setData] = useState([]);
  const [tiposComision, setTiposComision] = useState([]);
  const [diputados, setDiputados] = useState([]);
  const [respuestas, setRespuestas] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [comisionSeleccionado, setcomisionSeleccionado] = useState({
    id: "",
    nombre: "",
    tipo_comision: 0
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setcomisionSeleccionado((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  //Trae los data desde la api
  const peticionGet = async () => {
    await axios
      .get(baseUrl)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
//console.log(peticionGet);
  // Obtener comisiones de BD
  const tipoComisionGet = async () => {
    await axios
      .get("http://45.56.114.13:8000/api/ctl/tipocomision/")
      .then((response) => {
        setTiposComision(response.data);
      })
      .catch((error) => {
        console.log("ERROR: ", error);
      });
  };

  //const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;
  const peticionPost = async (values) => {
    console.log("VALUES: ", values);
    const headers = { "Content-Type": "application/json" };
    await axios
      .post(baseUrl, { nombre: values.nombre, tipo: values.tipo_comision }, headers)
      .then((response) => {
        setData(data.concat(response.data));
        abrirCerrarModalInsertar();
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // // Testeo de valores enviados desde el submit
  // const peticionPost = (values) => {
  //   console.log("VALUES: ", values);
  //   //console.log("X-CSRFToken: ", csrftoken);
  // };

  const peticionPut = async () => {
    const headers = { "Content-Type": "application/json" };
    await axios
      .put(
        baseUrl + "modificar/" + comisionSeleccionado.id + "/",
        { nombre: comisionSeleccionado.nombre, tipo: comisionSeleccionado.tipo_comision },
        headers
      )
      .then((response) => {
        var dataNueva = data;
        dataNueva.map((comision) => {
          if (comision.id === comisionSeleccionado.id) {
            comision.nombre = comisionSeleccionado.nombre;
            comision.tipo_comision = comisionSeleccionado.tipo_comision;
          }
        });
        setData(dataNueva);
        abrirCerrarModalEditar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const peticionDelete = async () => {
    await axios
      .delete(baseUrl + "modificar/" + comisionSeleccionado.id + "/")
      .then((response) => {
        setData(
          data.filter((comision) => comision.id !== comisionSeleccionado.id)
        );
        abrirCerrarModalEliminar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const seleccionarDiputado = (comision, caso) => {
    setcomisionSeleccionado({ id: comision.id, nombre: comision.nombre, tipo_comision: comision.tipo });
    caso === "Editar" ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };

  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };

  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };

  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  useEffect(() => {
    peticionGet();
    tipoComisionGet();
  }, []);

  useEffect(() => {
    if (modalInsertar === false || modalEditar === false)
      peticionGet();
  }, [modalInsertar, modalEditar]);

  //Modal para insertar una comision
  const [formularioEnviado, cambiarFormularioEnviado] = useState(false);
  const bodyInsertar = (
    <div className={styles.modal}>
      <h3>Agregar Comisión</h3>
      <br />
      <Formik
        initialValues={{
          nombre: "",
          tipo_comision: "",
        }}
        validate={(values) => {
          const errors = {};

          //   // Validacion nombre
          //   if (!values.nombre) {
          //     errors.nombre = "Por favor ingresa el nombre del diputado";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre)) {
          //     errors.nombre =
          //       "El nombre del diputado solo puede contener letras y espacios";
          //   }

          //   // Validacion nombre_comision
          //   if (!values.nombre_comision) {
          //     errors.nombre_comision =
          //       "Por favor ingresa la comisión a la que pertenece";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_comision)) {
          //     errors.nombre_comision =
          //       "El nombre de la comisión solo puede contener letras y espacios";
          //   }

          //   // Validacion de nombre_partido
          //   if (!values.nombre_partido) {
          //     errors.nombre_partido =
          //       "Por favor ingresa el partido político al que pertenece";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_partido)) {
          //     errors.nombre_partido =
          //       "El nombre del partido político solo puede contener letras y espacios";
          //   }

          //   // Validacion de periodo_legislatura_rango_fecha
          //   if (!values.periodo_legislatura_rango_fecha) {
          //     errors.periodo_legislatura_rango_fecha =
          //       "Por favor ingresa la fecha";
          //   } else if (
          //     !/^[0-9]{4}-[0-9]{4}$/.test(values.periodo_legislatura_rango_fecha)
          //   ) {
          //     errors.periodo_legislatura_rango_fecha =
          //       "El periodo de legislatura debe de contener el siguiente formato:####-####";
          //   }
          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          peticionPost(values);
          resetForm();
          console.log("Formulario enviado", values);
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          values,
        }) => (
          <Form>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor="nombre">Nombre</label>
                  <Field
                    type="text"
                    id="nombre"
                    name="nombre"
                    placeholder="Ingrese el nombre de la comisión"
                    className={styles.FieldMaterial}
                    validate={errors.nombre && touched.nombre}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.nombre && touched.nombre && (
                    <div className={styles.ErrorMessage}> {errors.nombre} </div>
                  )}
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor="tipo_comision">Tipo de comisión</label>
                  <Field
                    component="select"
                    id="tipo_comision"
                    name="tipo_comision"
                    multiple={false}
                    className={styles.FieldMaterial}
                  >
                    <option value="" className="text-secondary">
                      --- Tipo de comisión ---
                    </option>
                    {tiposComision.map((tipoComision) => (
                      <option
                        key={tipoComision.id}
                        value={tipoComision.id}
                        className={styles.FieldMaterial}
                      >
                        {tipoComision.texto_tipo}
                      </option>
                    ))}
                  </Field>
                  {errors.tipo_comision && touched.tipo_comision && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.tipo_comision}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <br />
                <Stack direction="row" spacing={2}>
                  <Button
                    variant='contained'
                    color="success"
                    onClick={() => peticionPost(values)}
                  >
                    Insertar
                  </Button>
                  <Button
                    variant='contained'
                    color="error"
                    onClick={() => abrirCerrarModalInsertar()}
                  >
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );

  //Modal para editar votacion
  const bodyEditar = (
    <div className={styles.modal}>
      <h3>Editar Comisión</h3>
      <Formik
        validate={(values) => {
          const errors = {};

          //   // Validacion nombre
          //   if (!values.nombre) {
          //     errors.nombre = "Por favor ingresa el nombre del diputado";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre)) {
          //     errors.nombre =
          //       "El nombre del diputado solo puede contener letras y espacios";
          //   }

          //   // Validacion nombre_comision
          //   if (!values.nombre_comision) {
          //     errors.nombre_comision =
          //       "Por favor ingresa la comisión a la que pertenece";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_comision)) {
          //     errors.nombre_comision =
          //       "El nombre de la comisión solo puede contener letras y espacios";
          //   }

          //   // Validacion de nombre_partido
          //   if (!values.nombre_partido) {
          //     errors.nombre_partido =
          //       "Por favor ingresa el partido político al que pertenece";
          //   } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_partido)) {
          //     errors.nombre_partido =
          //       "El nombre del partido político solo puede contener letras y espacios";
          //   }

          //   // Validacion de periodo_legislatura_rango_fecha
          //   if (!values.periodo_legislatura_rango_fecha) {
          //     errors.periodo_legislatura_rango_fecha =
          //       "Por favor ingresa la fecha";
          //   } else if (
          //     !/^[0-9]{4}-[0-9]{4}$/.test(values.periodo_legislatura_rango_fecha)
          //   ) {
          //     errors.periodo_legislatura_rango_fecha =
          //       "El periodo de legislatura debe de contener el siguiente formato:####-####";
          //   }
          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          peticionPut();
          resetForm();
          console.log("Formulario enviado");
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({ errors, touched, handleBlur, handleSubmit, values }) => (
          <Form onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor="nombre">Nombre</label>
                  <Field
                    type="text"
                    id="nombre"
                    name="nombre"
                    placeholder="Ingrese el nombre de la comisión"
                    className={styles.FieldMaterial}
                    validate={errors.nombre && touched.nombre}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={comisionSeleccionado && comisionSeleccionado.nombre}
                  />
                  {errors.nombre && touched.nombre && (
                    <div className={styles.ErrorMessage}> {errors.nombre} </div>
                  )}
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor="tipo_comision">Tipo de comisión</label>
                  <Field
                    component="select"
                    id="tipo_comision"
                    name="tipo_comision"
                    multiple={false}
                    className={styles.FieldMaterial}
                    value={comisionSeleccionado && comisionSeleccionado.tipo_comision}
                    onChange={handleChange}
                  >
                    {/* <option value="" className="text-secondary">
                      --- Tipo de comisión ---
                    </option> */}
                    {tiposComision.map((tipoComision) => (
                      <option
                        key={tipoComision.tipo_comision}
                        value={tipoComision.id}
                        className={styles.FieldMaterial}
                      >
                        {tipoComision.texto_tipo}
                      </option>
                    ))}
                  </Field>
                  {errors.tipo_comision && touched.tipo_comision && (
                    <div className={styles.ErrorMessage}>
                      {" "}
                      {errors.tipo_comision}{" "}
                    </div>
                  )}
                </FormGroup>
                <br />
                <br />
                <Stack direction="row" spacing={2}>
                  <Button
                    variant="contained"
                    color="success"
                    onClick={() => peticionPut(values)}
                  >
                    Editar
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={() => abrirCerrarModalEditar()}
                  >
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );

  //modal para eliminar una empresa
  const bodyEliminar = (
    <div className={styles.modal}>
      <p>
        Estás seguro que deseas eliminar la comisión{" "}
        <b>{comisionSeleccionado && comisionSeleccionado.nombre}</b>?{" "}
      </p>
      <div align="right">
        <Button color="secondary" onClick={() => peticionDelete()}>
          Sí
        </Button>
        <Button onClick={() => abrirCerrarModalEliminar()}>No</Button>
      </div>
    </div>
  );

  return (
    <>
      <div className={table.table}>
        <MaterialTable
          columns={columns}
          data={data}
          title="Comisiones"
          //Propiedades de los iconos eliminar y editar
          actions={[
            {
              icon:()=> <AddIcon/>,
              tooltip: "Agregar empresa",
              isFreeAction: true,
              onClick: (event, rowData) => abrirCerrarModalInsertar(),
            },
            {
              icon: ()=> <EditIcon/>,
              tooltip: "Editar Empresa",
              onClick: (event, rowData) =>
                seleccionarDiputado(rowData, "Editar"),
            },
            {
              icon: ()=> <DeleteIcon/>,
              tooltip: "Eliminar Empresa",
              onClick: (event, rowData) =>
                seleccionarDiputado(rowData, "Eliminar"),
            },
            {
              icon: () => <PictureAsPdfIcon />,// you can pass icon too
              tooltip: "Export to Pdf",
              onClick: () => downloadPdf(data),
              isFreeAction: true
            },
          ]}
          //Propiedades de la tabla
          options={{
            filtering: true,
            grouping: true,
            sorting: true,
            thirdSortClick: false,
            search: true,
            searchFieldAlignment: "right",
            searchAutoFocus: true,
            searchFieldVariant: "standard",
            paging: true,
            pageSizeOptions: [5, 10, 20, 25, 50, 100],
            pageSize: 5,
            paginationType: "stepped",
            showFirstLastPageButtons: false,
            paginationPosition: "bottom",
            addRowPosition: "first",
            actionsColumnIndex: -1,
            columnsButton: true,
          }}
          localization={{
            header: {
              actions: "Acciones",
            },
          }}
        />
        <Modal open={modalInsertar} onClose={abrirCerrarModalInsertar}>
          {bodyInsertar}
        </Modal>

        <Modal open={modalEditar} onClose={abrirCerrarModalEditar}>
          {bodyEditar}
        </Modal>

        <Modal open={modalEliminar} onClose={abrirCerrarModalEliminar}>
          {bodyEliminar}
        </Modal>
      </div>
    </>
  );
}

export default Comisiones;
