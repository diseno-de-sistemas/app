import { useState, useEffect } from 'react';
import axios from 'axios';
import MaterialTable from '@material-table/core';
import { Formik, Form, Field } from 'formik';
import { Modal, Grid, Input } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import { FormGroup } from '@mui/material';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import AddIcon from '@mui/icons-material/Add';
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

//Columnas de la tabla
const columns = [
  {
    title: 'Nombre',
    field: 'nombre',
    filterComponent: () => <Input placeholder='custom' />,
    grouping: true,
  },
  {
    title: 'Comisión',
    field: 'nombre_comision',
    filterComponent: () => <Input placeholder='custom' />,
    grouping: true,
    sorting: false,
  },
  {
    title: 'Partido',
    field: 'nombre_partido',
    filterComponent: () => <Input placeholder='custom' />,
    grouping: false,
    sorting: false,
  },
  {
    title: 'Periodo de legislatura',
    field: 'periodo_legislatura_rango_fecha',
    filtering: false,
    grouping: false,
    sorting: false,
  },
];

const downloadPdf = (dat) => {
  const doc = new jsPDF();
  doc.text('Diputados', 20, 10);
  doc.autoTable({
    theme: 'grid',
    columns: columns.map((col) => ({ ...col, dataKey: col.field })),
    body: dat,
  });
  doc.save('Diputados.pdf');
};

//url de la api
const baseUrl = 'http://45.56.114.13:8000/api/ctl/diputados/';

//Estilos del modal
const useStyles = makeStyles((theme) => ({
  modal: {
    position: 'absolute',
    width: 1000,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
  iconos: {
    cursor: 'pointer',
  },
  FieldMaterial: {
    width: '100%',
  },
  ErrorMessage: {
    color: '#FF0000',
    fontStyle: 'italic',
    fontFamily: 'Times New Roman',
    fontSize: '15px',
  },
}));

//Estilos para la DataTable
const tableStyles = makeStyles((theme) => ({
  table: {
    marginTop: theme.spacing(3),
    '& thead th': {
      fontWeight: '600',
      color: theme.palette.primary.main,
      backgroundColor: ' #d5d6dc ',
    },
    '& tbody td': {
      fontWeight: '300',
    },
    '& tbody tr:hover': {
      backgroundColor: '#fffbf2',
      cursor: 'pointer',
    },
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderBlockStyle: 'solid',
    borderBlockWidth: 'medium',
  },
}));

function Diputados() {
  const styles = useStyles();
  const table = tableStyles();
  const [data, setData] = useState([]);
  const [comisiones, setComisiones] = useState([]);
  const [partidos, setPartidos] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [periodosLegislatura, setPeriodosLegislatura] = useState([]);
  const [diputadoSeleccionado, setDiputadoSeleccionado] = useState({
    id: '',
    nombre: '',
    nombre_comision: '',
    nombre_partido: '',
    periodo_legislatura_rango_fecha: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setDiputadoSeleccionado((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  //Trae los data desde la api
  const peticionGet = async () => {
    await axios
      .get(baseUrl)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Obtener comisiones de BD
  const comisionesGet = async () => {
    await axios
      .get('http://45.56.114.13:8000/api/ctl/comisiones/')
      .then((response) => {
        setComisiones(response.data);
      })
      .catch((error) => {
        console.log('ERROR: ', error);
      });
  };

  // Obtener periodos de BD
  const periodosGet = async () => {
    await axios
      .get('http://45.56.114.13:8000/api/ctl/periodolegislatura/')
      .then((response) => {
        setPeriodosLegislatura(response.data);
      })
      .catch((error) => {
        console.log('ERROR: ', error);
      });
  };

  // Obtener partidos de BD
  const partidosGet = async () => {
    await axios
      .get('http://45.56.114.13:8000/api/ctl/partidopolitico/')
      .then((response) => {
        setPartidos(response.data);
      })
      .catch((error) => {
        console.log('ERROR: ', error);
      });
  };

  const peticionPost = async (values) => {
    console.log('VALUES: ', values);
    const { nombre_comision, nombre_partido, periodo_legislatura_rango_fecha, nombre } = values;
    const formData = {
      nombre,
      id_comision: nombre_comision,
      partido: nombre_partido,
      periodo_legislatura: periodo_legislatura_rango_fecha,
    };

    await axios
      .post(baseUrl, formData)
      .then((response) => {
        setData(data.concat(response.data));
        abrirCerrarModalInsertar();
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };


  const peticionPut = async () => {
    await axios
      .put(baseUrl + 'modificar/' + diputadoSeleccionado.id + '/', diputadoSeleccionado)
      .then((response) => {
        peticionGet();
        abrirCerrarModalEditar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const peticionDelete = async () => {
    await axios
      .delete(baseUrl + 'modificar/' + diputadoSeleccionado.id + '/')
      .then((response) => {
        setData(data.filter((diputado) => diputado.id !== diputadoSeleccionado.id));
        abrirCerrarModalEliminar();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const seleccionarDiputado = (diputado, caso) => {
    setDiputadoSeleccionado(diputado);
    caso === 'Editar' ? abrirCerrarModalEditar() : abrirCerrarModalEliminar();
  };

  const abrirCerrarModalInsertar = () => {
    setModalInsertar(!modalInsertar);
  };

  const abrirCerrarModalEditar = () => {
    setModalEditar(!modalEditar);
  };

  const abrirCerrarModalEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  useEffect(() => {
    peticionGet();
    comisionesGet();
    partidosGet();
    periodosGet();
  }, []);

  //Modal para insertar un diputado
  const [formularioEnviado, cambiarFormularioEnviado] = useState(false);

  const bodyInsertar = (
    <div className={styles.modal}>
      <h3>Agregar Diputado</h3>
      <br />
      <Formik
        initialValues={{
          nombre: '',
          nombre_comision: '',
          nombre_partido: '',
          periodo_legislatura_rango_fecha: '',
        }}
        validate={(values) => {
          const errors = {};

          // Validacion nombre
          if (!values.nombre) {
            errors.nombre = 'Por favor ingresa el nombre del diputado';
          } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre)) {
            errors.nombre = 'El nombre del diputado solo puede contener letras y espacios';
          }

          // Validacion nombre_comision
          if (!values.nombre_comision) {
            errors.nombre_comision = 'Por favor ingresa la comisión a la que pertenece';
          } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_comision)) {
            errors.nombre_comision = 'El nombre de la comisión solo puede contener letras y espacios';
          }

          // Validacion de nombre_partido
          if (!values.nombre_partido) {
            errors.nombre_partido = 'Por favor ingresa el partido político al que pertenece';
          } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_partido)) {
            errors.nombre_partido = 'El nombre del partido político solo puede contener letras y espacios';
          }

          // Validacion de periodo_legislatura_rango_fecha
          if (!values.periodo_legislatura_rango_fecha) {
            errors.periodo_legislatura_rango_fecha = 'Por favor ingresa la fecha';
          } else if (!/^[0-9]{4}-[0-9]{4}$/.test(values.periodo_legislatura_rango_fecha)) {
            errors.periodo_legislatura_rango_fecha =
              'El periodo de legislatura debe de contener el siguiente formato:####-####';
          }
          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          peticionPost(values);
          resetForm();
          console.log('Formulario enviado', values);
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({ errors, touched, handleBlur, handleChange, handleSubmit, values }) => (
          <Form>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor='nombre'>Nombre</label>
                  <Field
                    type='text'
                    id='nombre'
                    name='nombre'
                    placeholder='Ingrese el nombre del diputado'
                    className={styles.FieldMaterial}
                    validate={errors.nombre && touched.nombre}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors.nombre && touched.nombre && <div className={styles.ErrorMessage}> {errors.nombre} </div>}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='nombre_comision'>Seleccionar comisión</label>
                  <Field
                    component='select'
                    id='nombre_comision'
                    name='nombre_comision'
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder='--- Seleccione comisión ---'
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione comisión ---
                    </option>
                    {comisiones.map((comision) => (
                      <option key={comision.nombre} value={comision.id} className={styles.FieldMaterial}>
                        {comision.nombre}
                      </option>
                    ))}
                  </Field>        
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor='nombre_partido'>Seleccione el partido político al que pertenece</label>
                  <Field
                    component='select'
                    id='nombre_partido'
                    name='nombre_partido'
                    multiple={false}
                    className={styles.FieldMaterial}
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione partido político ---
                    </option>
                    {partidos.map((partido) => (
                      <option key={partido.id} value={partido.id} className={styles.FieldMaterial}>
                        {partido.nombre}
                      </option>
                    ))}
                  </Field>
                </FormGroup>

                <FormGroup>
                  <label htmlFor='nombre_comision'>Seleccione temporada gestión</label>
                  <Field
                    component='select'
                    id='periodo_legislatura_rango_fecha'
                    name='periodo_legislatura_rango_fecha'
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder='--- Seleccione temporada gestión ---'
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione periodo de legislatura ---
                    </option>
                    {periodosLegislatura.map((sesionPlenaria) => (
                      <option key={sesionPlenaria.id} value={sesionPlenaria.id} className={styles.FieldMaterial}>
                        {sesionPlenaria.rango_fecha}
                      </option>
                    ))}
                  </Field>
                </FormGroup>
                <br />
                <br />
                <Stack direction='row' spacing={2}>
                  <Button variant='contained' color='success' onClick={() => peticionPost(values)}>
                    Insertar
                  </Button>
                  <Button variant='contained' color='error' onClick={() => abrirCerrarModalInsertar()}>
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );

  //Modal para editar un diputado
  const bodyEditar = (
    <div className={styles.modal}>
      <h3>Editar Diputado</h3>
      <Formik
        validate={(values) => {
          const errors = {};

          // // Validacion nombre
          // if (!values.nombre) {
          //   errors.nombre = 'Por favor ingresa el nombre del diputado';
          // } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre)) {
          //   errors.nombre = 'El nombre del diputado solo puede contener letras y espacios';
          // }

          // // Validacion nombre_comision
          // if (!values.nombre_comision) {
          //   errors.nombre_comision = 'Por favor ingresa la comisión a la que pertenece';
          // } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_comision)) {
          //   errors.nombre_comision = 'El nombre de la comisión solo puede contener letras y espacios';
          // }

          // // Validacion de nombre_partido
          // if (!values.nombre_partido) {
          //   errors.nombre_partido = 'Por favor ingresa el partido político al que pertenece';
          // } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.nombre_partido)) {
          //   errors.nombre_partido = 'El nombre del partido político solo puede contener letras y espacios';
          // }

          // // Validacion de periodo_legislatura_rango_fecha
          // if (!values.periodo_legislatura) {
          //   errors.periodo_legislatura = 'Por favor ingresa la fecha';
          // } else if (!/^[0-9]{4}-[0-9]{4}$/.test(values.periodo_legislatura)) {
          //   errors.periodo_legislatura =
          //     'El periodo de legislatura debe de contener el siguiente formato:####-####';
          // }

          return errors;
        }}
        onSubmit={(values, { resetForm }) => {
          peticionPut();
          resetForm();
          console.log('Formulario enviado');
          cambiarFormularioEnviado(true);
          setTimeout(() => cambiarFormularioEnviado(false), 5000);
        }}
      >
        {({ errors, touched, handleBlur, handleSubmit, values }) => (
          <Form onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <FormGroup>
                  <label htmlFor='nombre'>Nombre</label>
                  <Field
                    type='text'
                    id='nombre'
                    name='nombre'
                    placeholder='Ingrese el nombre del diputado'
                    className={styles.FieldMaterial}
                    validate={errors.nombre && touched.nombre}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={diputadoSeleccionado && diputadoSeleccionado.nombre}
                  />
                  {/* {errors.nombre && touched.nombre && <div className={styles.ErrorMessage}> {errors.nombre} </div>} */}
                </FormGroup>

                <FormGroup>
                  <label htmlFor='nombre_comision'>Seleccione comisión a la que pertenece</label>
                  <Field
                    component='select'
                    id='id_comision'
                    name='id_comision'
                    multiple={false}
                    className={styles.FieldMaterial}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={diputadoSeleccionado && diputadoSeleccionado.id_comision}
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione comisión ---
                    </option>
                    {comisiones.map((comision) => (
                      <option key={comision.id_comision} value={comision.id} className={styles.FieldMaterial}>
                        {comision.nombre}
                      </option>
                    ))}
                    {/* {errors.nombre_comision && touched.nombre_comision && (
                      <div className={styles.ErrorMessage}> {errors.nombre_comision} </div>
                    )} */}
                  </Field>
                </FormGroup>
              </Grid>

              <Grid item xs={6} md={6}>
                <FormGroup>
                  <label htmlFor='nombre_partido'>Seleccione el partido político al que pertenece</label>
                  <Field
                    component='select'
                    id='partido'
                    name='partido'
                    multiple={false}
                    className={styles.FieldMaterial}
                    value={diputadoSeleccionado && diputadoSeleccionado.partido}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione partido político ---
                    </option>
                    {partidos.map((partido) => (
                      <option key={partido.id} value={partido.id} className={styles.FieldMaterial}>
                        {partido.nombre}
                      </option>
                    ))}
                  </Field>
                  {errors.nombre_partido && touched.nombre_partido && (
                    <div className={styles.ErrorMessage}> {errors.nombre_partido} </div>
                  )}
                </FormGroup>

                {/* <FormGroup>
                  <label htmlFor='periodo_legislatura_rango_fecha'>Periodo de legislatura</label>
                  <Field
                    type='text'
                    id='periodo_legislatura_rango_fecha'
                    name='periodo_legislatura_rango_fecha'
                    placeholder='2021-2024'
                    className={styles.FieldMaterial}
                    validate={errors.periodo_legislatura_rango_fecha && touched.periodo_legislatura_rango_fecha}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={diputadoSeleccionado && diputadoSeleccionado.periodo_legislatura_rango_fecha}
                  />
                  {errors.periodo_legislatura_rango_fecha && touched.periodo_legislatura_rango_fecha && (
                    <div className={styles.ErrorMessage}> {errors.periodo_legislatura_rango_fecha} </div>
                  )}
                </FormGroup> */}

                <FormGroup>
                  <label htmlFor='nombre_comision'>Seleccione temporada gestión</label>
                  <Field
                    component='select'
                    id='periodo_legislatura'
                    name='periodo_legislatura'
                    multiple={false}
                    className={styles.FieldMaterial}
                    placeholder='--- Seleccione temporada gestión ---'
                    value={diputadoSeleccionado && diputadoSeleccionado.periodo_legislatura}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    <option value='' className='text-secondary'>
                      --- Seleccione periodo de legislatura ---
                    </option>
                    {periodosLegislatura.map((sesionPlenaria) => (
                      <option key={sesionPlenaria.id} value={sesionPlenaria.id} className={styles.FieldMaterial}>
                        {sesionPlenaria.rango_fecha}
                      </option>
                    ))}
                  </Field>
                </FormGroup>
                <br />
                <br />
                <Stack direction='row' spacing={2}>
                  <Button variant='contained' color='success' onClick={() => peticionPut(values)}>
                    Editar
                  </Button>
                  <Button variant='contained' color='error' onClick={() => abrirCerrarModalEditar()}>
                    Cancelar
                  </Button>
                </Stack>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );

  //modal para eliminar una empresa
  const bodyEliminar = (
    <div className={styles.modal}>
      <p>
        Estás seguro que deseas eliminar el diputado <b>{diputadoSeleccionado && diputadoSeleccionado.nombre}</b>?{' '}
      </p>
      <div align='right'>
        <Button color='secondary' onClick={() => peticionDelete()}>
          Sí
        </Button>
        <Button onClick={() => abrirCerrarModalEliminar()}>No</Button>
      </div>
    </div>
  );

  return (
    <>
      <div className={table.table}>
        <MaterialTable
          columns={columns}
          data={data}
          title='Diputados'
          //Propiedades de los iconos eliminar y editar
          actions={[
            {
              icon: () => <AddIcon />,
              tooltip: 'Agregar empresa',
              isFreeAction: true,
              onClick: (event, rowData) => abrirCerrarModalInsertar(),
            },
            {
              icon: () => <EditIcon />,
              tooltip: 'Editar Empresa',
              onClick: (event, rowData) => seleccionarDiputado(rowData, 'Editar'),
            },
            {
              icon: () => <DeleteIcon />,
              tooltip: 'Eliminar Empresa',
              onClick: (event, rowData) => seleccionarDiputado(rowData, 'Eliminar'),
            },
            {
              icon: () => <PictureAsPdfIcon />, // you can pass icon too
              tooltip: 'Export to Pdf',
              onClick: () => downloadPdf(data),
              isFreeAction: true,
            },
          ]}
          //Propiedades de la tabla
          options={{
            filtering: true,
            grouping: true,
            sorting: true,
            thirdSortClick: false,
            search: true,
            searchFieldAlignment: 'right',
            searchAutoFocus: true,
            searchFieldVariant: 'standard',
            paging: true,
            pageSizeOptions: [5, 10, 20, 25, 50, 100],
            pageSize: 10,
            paginationType: 'stepped',
            showFirstLastPageButtons: false,
            paginationPosition: 'bottom',
            addRowPosition: 'first',
            actionsColumnIndex: -1,
            columnsButton: true,
          }}
          localization={{
            header: {
              actions: 'Acciones',
            },
          }}
        />
        <Modal open={modalInsertar} onClose={abrirCerrarModalInsertar}>
          {bodyInsertar}
        </Modal>

        <Modal open={modalEditar} onClose={abrirCerrarModalEditar}>
          {bodyEditar}
        </Modal>

        <Modal open={modalEliminar} onClose={abrirCerrarModalEliminar}>
          {bodyEliminar}
        </Modal>
      </div>
    </>
  );
}

export default Diputados;
